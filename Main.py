# New Modules
import sys
import opentumflex

# Import functions
# Flexibility Models
from flex_models.OpenTUMFlex_ufs import example
from flex_models.Ulbig import flex_model_Ulbig
from flex_models.FlexOffer import flex_model_FlexOffer

# Visualisation
from visualisation import flexibility_visualisation_OpenTUM
from visualisation import flexibility_visualisation_Ulbig
from visualisation import flexibility_visualisation_FlexOffer

# Aggregation
# from aggregation import Agg_main

# Results
from results import save_results

# ----------Prompt----------
solver = 'glpk'  # Select between 'gurobi' and 'glpk'
if solver == 'gurobi' and 'gurobipy' not in sys.modules:
    print("[Error] Solver is 'gurobi' but 'gurobipy' is not in sys.modules")
    sys.exit(1)
elif solver not in ('gurobi', 'glpk'):
    print(
        f"[Error] Solver '{solver}' is not available, choose 'gurobi' or 'glpk' please")
    sys.exit(1)

# Unit commitment optimisation
optimisation = True  # Decide if OpenTUMFlex unit commitment opitmisation should be executed before
# flexibility calculation or if results from previous simulations should be used.

# OpenTUMFlex
OpenTUM_flex_calc = True
OpenTUM_visu = True
OpenTUM_Agg = False
OpenTUM_save = False

# Ulbig
Ulbig_flex_calc = True
Ulbig_visu = True
Ulbig_Agg = False
Ulbig_save = False

# FlexOffer
FlexOffer_calc = True
FlexOffer_visu = True
FlexOffer_Agg = False

# Dependency-based FlexOffer
DFO_calc = True  # Only possible with FlexOffer_calc = True
DFO_visu = True
DFO_Agg = False

FlexOffer_DFO_save = False

# Aggregation
Agg_visu = False

# -----------------------------------------------------------------------------
# OpenTUMFlex flexibility model
OpenTUM_res = example.OpenTUM(opentumflex.scenario_apartment, solver,
                              optimisation, OpenTUM_flex_calc)

visu_OpenTUM_res = flexibility_visualisation_OpenTUM.OpenTUM_flex_visu(
    OpenTUM_res, OpenTUM_visu)

save_results.save_OpenTUM_res(OpenTUM_res, OpenTUM_save)

# -----------------------------------------------------------------------------
# Ulbig flexibility model
Ulbig_res = flex_model_Ulbig.flex_Ulbig(OpenTUM_res, Ulbig_flex_calc)

visu_Ulbig_res = flexibility_visualisation_Ulbig.Ulbig_flex_visu(Ulbig_res,
                                                                 Ulbig_visu,
                                                                 OpenTUM_res)

save_results.save_Ulbig_res(Ulbig_res, Ulbig_save)

# -----------------------------------------------------------------------------
# FlexOffer flexibility model
FlexOffer_res = flex_model_FlexOffer.flex_FlexOffer(OpenTUM_res,
                                                    FlexOffer_calc, DFO_calc,
                                                    solver)

flexibility_visualisation_FlexOffer.FlexOffer_flex_visu(OpenTUM_res,
                                                        FlexOffer_res,
                                                        FlexOffer_visu,
                                                        DFO_visu)

save_results.save_FlexOffer_res(FlexOffer_res, FlexOffer_DFO_save, DFO_calc)

# -----------------------------------------------------------------------------
# Aggregation

# Agg_main.agg(OpenTUM_res, Ulbig_res, FlexOffer_res, OpenTUM_Agg, Ulbig_Agg,
#              FlexOffer_Agg, DFO_Agg, Agg_visu)
