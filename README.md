# Unified flexibility modelling

An python-based Open Source tool including four different flexibility models that map the flexibility of different power devices in a unified form. 

# Introduction

A growing amount of renewables and electrical consumers lead to a increasing demand for flexibility in modern and distributed energy supply systems. 
Aggregated flexibility from small-scale and distributed power devices can contribute to fill the opening flexibility gap.
The harnessing of flexibility from different small-scale power devices through aggregation necessitates the mapping of their flexibility in a unified form.

# Description

This tool combines the unified flexibility modelling of different power devices based on four different approaches introduced in [OpenTUMFlex](https://github.com/tum-ewk/OpenTUMFlex), [Flexibility Trinity](https://ieeexplore.ieee.org/document/6344676), [FlexOffers](https://dl.acm.org/doi/10.1145/3077839.3077850) and [Dependency-based FlexOffers](https://dl.acm.org/doi/10.1145/2934328.2934339).
The included visualisation allows the investigation of the differing modelling results, which show the advantages and disadvantages of the implemented models.
By linking to the [FreqMatch-Market](https://gitlab.com/digitalized-energy-systems/models/freqmatchmarket) it is also possible to aggregate the modeled flexibility to different power market products.

# Program structure

As shown in the process diagram below, the data base for the simulations is mainly delivered by the built-in OpenTUMFlex Model. 
The parameters of five different power devices and their optimal operation plans, resulting from a previous optimization, are used as input for the four flexibility modells. 
The following devices are included: Photovoltaik, battery storage, electric vehicle, heat pump, combined heat and power unit

<img src="/images/Program_structure.PNG"  width="80%">

# Installation
## Windows (Anaconda)

Open **Anaconda Powershell Prompt** as **ADMIN**

### Clone and enter repository

<pre>
conda install git <b>(if required)</b>
git clone https://gitlab.com/digitalized-energy-systems/scenarios/unified_flex_scenario.git
cd unified_flex_scenario
</pre>

### Run batch script 
(Default solver is OpenSource solver GLPK. If Gurobi license is available (Commercial or Academic) comment in respective lines for gurobi installation and adjust the selected solver in the input prompt for faster execution of the program.)

<pre>
.\setup_w64.bat <b>for Win32:</b> .\setup_w32.bat
</pre>

### Run program

After running the script you have to activate the virtual environment in order to be able to run the program:

``` 
conda activate ufs-venv
python Main.py
``` 


## Linux

### Clone and enter repository
```
git clone https://gitlab.com/digitalized-energy-systems/scenarios/unified_flex_scenario.git
cd unified_flex_scenario/
```
### Run shell script 
(Default solver is OpenSource solver GLPK. If Gurobi license is available (Commercial or Academic) comment in respective lines for gurobi installation and adjust the selected solver in the input prompt for faster execution of the program.)

The bash script was tested under Ubuntu and should work for other Debian based distributions as well. For different distributions you have to change at least "apt" to your package manager.

```
bash -i setup_linux.sh
```
### Run program

After running the script you have to activate the virtual environment in order to be able to run the program:

```
source ufs-venv/bin/activate
python Main.py
```

# Test your installation

If your installation was successful, after running Main.py, the following pdf-files should have opened, showing the simulation results of all four modeling approaches: 

<img src="/images/Result_PDFs.PNG"  width="80%">

During the execution of the program the following errors may appear in your console:

<img src="/images/Console_Unsaved_Issues.PNG"  width="100%">

These errors are not affecting the execution of the program and their solution is being worked on.


# Handle input prompt

**1.** Open Main.py.

**2.** Select prefered/available solver (GLPK or Gurobi).

**3.** Decide if OpenTUMFlex-Unit Commitment Problem should be solved prior to flexibility modelling. Can be turned off, if results from previous simulations can be found in '\flex_models\OpenTUMFlex-ufs\output'.

**4.** Select the flexibility models that you want to simulate.

**5.** Visualising and Saving the modelling results can be turned on/off optionally.

**6.** The Aggregation of modelling results by the FreqMatch-Market can be turned on by activating the corresponding lines => Aggregation product definition in '/aggregation/Flex2Agg.py'.

