import csv
import numpy as np
import datetime


def write_agg_to_csv(agg, filepath):
    days = [datetime.date(2020, 11, 24), datetime.date(2020, 11, 25)]
    nr_of_agents = len(agg)
    blocks_per_day = 24 * 4  # 15min blocks
    price_lower_power = 400  # cents
    price_upper_power = 800
    price_lower_reserve = 25
    price_upper_reserve = 100
    with open(filepath, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow([i for i in range(nr_of_agents * 4)])
        for day in days:
            for a in range(blocks_per_day):
                row = []
                for b, dev in enumerate(agg):
                    price_lower = price_lower_power if b < len(agg) / 2 else price_lower_reserve
                    price_upper = price_upper_power if b < len(agg) / 2 else price_upper_reserve
                    for sign in ["POS", "NEG"]:
                        row.append(agg[dev][sign][a])
                        row.append(np.random.randint(price_lower, price_upper))
                writer.writerow(row)
