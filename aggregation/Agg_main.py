import datetime
import os
import sys
import pickle

from aggregation import Flex2Agg
from aggregation.Agg2csv import write_agg_to_csv
from fmm.config_parser import ConfigParser
from fmm.mosaik_simulator import run_simulation
from fmm.visualization import visualize

from visualisation import aggregation_visualisation


# Aggregation of calculated flexibility
# Five different aggregation products are possible, that are differing regarding their length
def agg(OpenTUM_res, Ulbig_res, FlexOffer_res, OpenTUM_Agg, Ulbig_Agg, FlexOffer_Agg,
        DFO_Agg, Agg_visu):
    # product information for aggregation prompt
    product_info = {}

    # 0: SRL; 1: MRL; 2: DAY_AHEAD; 3: INTRADAY_AUCTION; 4: INTRADAY_TRADE
    product_info['Market_type'] = 0
    product_info['day'] = datetime.date(2020, 11, 25)   # exemplary date that is used at FreqMatch, don't change!
    product_info['interval'] = 5                        # day of 24 hours is split into (blocks/4) hour intervals
    product_info['positive'] = False                    # positive or negative power

    # product length depending on Market Type
    if product_info['Market_type'] == 0 or product_info['Market_type'] == 1:
        product_info['blocks'] = 16
    elif product_info['Market_type'] == 3 or product_info['Market_type'] == 4:
        product_info['blocks'] = 1
    elif product_info['Market_type'] == 2:
        product_info['blocks'] = 4

    # aggregation processes
    if OpenTUM_Agg or Ulbig_Agg or FlexOffer_Agg or DFO_Agg:
        # modification of modelled flexibility to fit into aggregation format
        AggFlex_res = Flex2Agg.Flex2Agg_calc(OpenTUM_res, Ulbig_res, FlexOffer_res,
                                             OpenTUM_Agg, Ulbig_Agg, FlexOffer_Agg,
                                             DFO_Agg)

        # select optimal flexibilty offers for each involved plant
        AggFlex = Flex2Agg.Flex_select(product_info, AggFlex_res, OpenTUM_Agg,
                                       Ulbig_Agg, FlexOffer_Agg, DFO_Agg)

        # create csv file from aggregation
        filepath = os.path.join("aggregation", "flexlist.csv")
        write_agg_to_csv(AggFlex, filepath)

        # call fmm directly...
        prefix = os.path.join("aggregation", "freqmatch-market", "")
        sys.stdout = open(os.devnull, 'w')  # mute upcoming prints
        configuration = ConfigParser(path_prefix=prefix)
        configuration.nr_of_agents = len(AggFlex)
        configuration.flexlists_random = False
        configuration.flexlists_csv_path = filepath
        configuration.debug = False
        if 15 % configuration.step_size_mosaik != 0:
            print("[error] step size in mosaik has to be a factor of 15!")
            return
        run_simulation(configuration)

        # visualization by ufs
        with open(prefix + os.path.join("fmm", "sim_results"), "rb") as stored_results:
            simulation_results = pickle.load(stored_results)
            flex_lists = simulation_results["flexlists_final"]
            day = '2020-11-25'
            flex_final = {}
            for i, dev in enumerate(AggFlex):
                flex_final[dev + '_final'] = {
                    'POS': [flex_lists[i][day]['POS'][t].amount for t in range(len(flex_lists[i][day]['POS']))],
                    'NEG': [flex_lists[i][day]['NEG'][t].amount for t in range(len(flex_lists[i][day]['NEG']))]
                }
            aggregation_visualisation.Flex_Agg_visu(product_info, Agg_visu, AggFlex, flex_final, OpenTUM_res)

        # visualization by fmm
        try:
            visualize(path_prefix="aggregation/freqmatch-market/")
        except FileNotFoundError:
            print("[error] File sim_results not found")
