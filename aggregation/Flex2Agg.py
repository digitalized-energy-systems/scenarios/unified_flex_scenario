import numpy as np


def Flex2Agg_calc(OpenTUM_res, Ulbig_res, FlexOffer_res, OpenTUM_Agg,
                  Ulbig_Agg, FlexOffer_Agg, DFO_Agg):
    # Transform Flexibility into Aggregation Format
    # Time factor: timesteps per hour
    t_fac = OpenTUM_res['time_data']['t_inval'] / 60

    AggFlex_res = {}

# -----------------------------------------------------------------------------
    # OpenTUM
    if OpenTUM_Agg:
        AggFlex_OpenTUM = {}

        for device in OpenTUM_res['flexopts']:

            Flex_dev = {}
            Flex_pos = {}
            Flex_neg = {}
            Flex = OpenTUM_res['flexopts'][device]

            for t_step in range(len(Flex)):

                Flex_P = np.zeros((len(Flex)))
                Flex_N = np.zeros((len(Flex)))
                f_tspan_P = 0
                f_tspan_N = 0

                # calculate flexibility offer duration in 15 min blocks
                if Flex['Pos_E'][t_step] > 0:
                    f_tspan_P = int(round(Flex['Pos_E'][t_step] / Flex['Pos_P'][t_step] / t_fac))

                if Flex['Neg_E'][t_step] < 0:
                    f_tspan_N = int(round(Flex['Neg_E'][t_step] / Flex['Neg_P'][t_step] / t_fac))

                # fill flex arrays
                Flex_P[t_step:t_step + f_tspan_P] = Flex['Pos_P'][t_step]
                Flex_N[t_step:t_step + f_tspan_N] = Flex['Neg_P'][t_step]

                # fill dict t_step wise
                Flex_pos[str(t_step)] = Flex_P
                Flex_neg[str(t_step)] = Flex_N

            # fill dict device wise
            Flex_dev['POS'] = Flex_pos
            Flex_dev['NEG'] = Flex_neg
            AggFlex_OpenTUM[str(device)] = Flex_dev

        AggFlex_res['AggFlex_OpenTUM'] = AggFlex_OpenTUM

# -----------------------------------------------------------------------------
    # Ulbig
    if Ulbig_Agg:
        AggFlex_Ulbig = {}

        for device in Ulbig_res['ulbig_flex']:

            Flex_dev = {}
            Flex_pos = {}
            Flex_neg = {}
            Flex = Ulbig_res['ulbig_flex'][device]

            if Flex['device_type'] == 'storage':
                posflex = Flex['pi_gen_pos']
                negflex = -Flex['pi_load_pos']

            if Flex['device_type'] == 'generator':
                posflex = Flex['pi_gen_pos']
                negflex = Flex['pi_gen_neg']

            if Flex['device_type'] == 'load':
                posflex = -Flex['pi_load_neg']
                negflex = -Flex['pi_load_pos']

            for t_step in range(int(24 / t_fac)):

                Flex_P = np.zeros((int(24 / t_fac)))
                Flex_N = np.zeros((int(24 / t_fac)))

                # fill flex arrays
                Flex_P[t_step] = posflex[t_step]
                Flex_N[t_step] = negflex[t_step]

                # fill dict t_step wise
                Flex_pos[str(t_step)] = Flex_P
                Flex_neg[str(t_step)] = Flex_N

            # fill dict device wise
            Flex_dev['POS'] = Flex_pos
            Flex_dev['NEG'] = Flex_neg
            AggFlex_Ulbig[str(device)] = Flex_dev

        AggFlex_res['AggFlex_Ulbig'] = AggFlex_Ulbig


# -----------------------------------------------------------------------------
    # FlexOffer
    if FlexOffer_Agg:
        AggFlex_FlexOffer = {}

        for device in FlexOffer_res:

            Flex_dev = {}
            Flex_neg = {}
            FO_Flex = FlexOffer_res[device]['FlexOffers']

            for index, FO in enumerate(FO_Flex):

                # latest and earliest start time
                es = FO_Flex[FO]['t_es']
                ls = FO_Flex[FO]['t_ls']

                # iterate through time flexibility
                for t_step_start in range(es, ls + 1):

                    if device == 'ev':

                        Flex_N = np.zeros((int(24 / t_fac)))

                        # calculate flex for every time step of FlexOffer
                        for t_step_flex in range(len(FO_Flex[FO]['p'])):

                            Flex_N[t_step_start + t_step_flex] = (FO_Flex[FO]['p'][t_step_flex][0]
                                                                  - FO_Flex[FO]['p'][t_step_flex][1]) / t_fac

                        Flex_neg[str(t_step_start)] = Flex_N

                    if device == 'hp':

                        # calculate flex for every time step of FlexOffer
                        for t_step_flex in range(len(FO_Flex[FO]['p'])):

                            Flex_N = np.zeros((int(24 / t_fac)))
                            Flex_N[t_step_start + t_step_flex] = (FO_Flex[FO]['p'][t_step_flex][0]
                                                                  - FO_Flex[FO]['p'][t_step_flex][1]) / t_fac
                            Flex_neg[str(t_step_flex)] = Flex_N

            Flex_dev['NEG'] = Flex_neg
            AggFlex_FlexOffer[str(device)] = Flex_dev

        AggFlex_res['AggFlex_FlexOffer'] = AggFlex_FlexOffer

# -----------------------------------------------------------------------------
    # DFO
    if DFO_Agg:
        AggFlex_DFO = {}

        for device in FlexOffer_res:

            Flex_dev = {}
            Flex_neg = {}
            DFO_Flex_powst = {}
            DFO_Flex = FlexOffer_res[device]['DFOs']

            for index, DFO in enumerate(DFO_Flex):

                # latest and earliest start time
                es = FlexOffer_res[device]['FlexOffers']['FlexOffer_' + str(index)]['t_es']
                ls = FlexOffer_res[device]['FlexOffers']['FlexOffer_' + str(index)]['t_ls']
                le = (FlexOffer_res[device]['FlexOffers']['FlexOffer_' + str(index)]['t_ls']
                      + len(DFO_Flex[DFO]))

                # iterate through time flexibility
                for t_step_start in range(es, ls + 1):

                    # iterate through possible power states
                    for pow_state in range(len(DFO_Flex[DFO][0])):

                        Flex_N = np.zeros((int(24 / t_fac)))

                        # fill flex array
                        for t_step_flex in DFO_Flex[DFO]:

                            if t_step_flex == 0 and device == 'ev':
                                Flex_N[t_step_start + t_step_flex] = round((-DFO_Flex[DFO][t_step_flex]['v_max'][pow_state]
                                                                            + DFO_Flex[DFO][t_step_flex]['v_min'][0])
                                                                           / t_fac, 6)
                            elif (t_step_flex + 1) == le - ls:
                                Flex_N[t_step_start + t_step_flex] = round(Flex_N[t_step_start + t_step_flex - 1], 6)
                            else:
                                Flex_N[t_step_start + t_step_flex] = round((-(DFO_Flex[DFO][t_step_flex + 1]['d'][pow_state]
                                                                              - DFO_Flex[DFO][t_step_flex]['d'][pow_state])
                                                                            + DFO_Flex[DFO][t_step_flex]['v_min'][0])
                                                                           / t_fac, 6)

                        # fill dict power wise
                        DFO_Flex_powst[str(t_step_start) + '_' + str(pow_state)] = Flex_N

                    # fill dict t_step wise
                    Flex_neg[str(t_step_start)] = DFO_Flex_powst
                    DFO_Flex_powst = {}

            # fill dict device wise
            Flex_dev['NEG'] = Flex_neg
            AggFlex_DFO[str(device)] = Flex_dev

        AggFlex_res['AggFlex_DFO'] = AggFlex_DFO

    return AggFlex_res


def Flex_select(product_info, AggFlex_res, OpenTUM_Agg, Ulbig_Agg, FlexOffer_Agg, DFO_Agg):
    # Select best flexibility offers for chosen time interval, depending on offerable energy amount
    # Assign variables with product information
    # day = product_info['day']
    interval = product_info['interval']
    # market = product_info['Market_type']
    blocks = product_info['blocks']
    flex_interval = np.add(np.array([0, blocks]), blocks * interval)
    AggFlex = {}

# -----------------------------------------------------------------------------
    # OpenTUM
    if OpenTUM_Agg:
        for device in AggFlex_res['AggFlex_OpenTUM']:

            DevFlex = {}
            max_flex_sum = 0

            # iterate through time steps
            for t_step in AggFlex_res['AggFlex_OpenTUM'][device]['NEG']:

                flex_plan = AggFlex_res['AggFlex_OpenTUM'][device]['NEG'][t_step]
                flex_sum = sum(flex_plan[flex_interval[0]:flex_interval[1]])

                # maximal flexibility check
                if flex_sum < max_flex_sum:
                    max_flex_sum = flex_sum
                    DevFlex['NEG'] = flex_plan

            if max_flex_sum == 0:
                DevFlex['NEG'] = np.zeros((96))

            max_flex_sum = 0

            # iterate through time steps
            for t_step in AggFlex_res['AggFlex_OpenTUM'][device]['POS']:

                flex_plan = AggFlex_res['AggFlex_OpenTUM'][device]['POS'][t_step]
                flex_sum = sum(flex_plan[flex_interval[0]:flex_interval[1]])

                # maximal flexibility check
                if flex_sum > max_flex_sum:
                    max_flex_sum = flex_sum
                    DevFlex['POS'] = flex_plan

            if max_flex_sum == 0:
                DevFlex['POS'] = np.zeros((96))

            AggFlex['OpenTUM_' + str(device)] = DevFlex


# -----------------------------------------------------------------------------
    # Ulbig
    if Ulbig_Agg:
        if blocks == 4:
            t_neg = 0
            t_pos = 0
        for device in AggFlex_res['AggFlex_Ulbig']:

            DevFlex = {}
            max_flex_sum = 0

            # iterate through time steps
            for t_step in AggFlex_res['AggFlex_Ulbig'][device]['NEG']:

                flex_plan = AggFlex_res['AggFlex_Ulbig'][device]['NEG'][t_step]
                if blocks == 4:
                    flex_sum = sum(flex_plan[flex_interval[0] + (t_neg):flex_interval[0] + t_neg + 1])
                else:
                    flex_sum = sum(flex_plan[flex_interval[0]:flex_interval[1]])

                # maximal flexibility check
                if flex_sum < max_flex_sum:
                    max_flex_sum = flex_sum
                    DevFlex['NEG'] = flex_plan

            if max_flex_sum == 0:
                DevFlex['NEG'] = np.zeros((96))
            elif blocks == 4 and max_flex_sum != 0:
                t_neg = t_neg + 1

            max_flex_sum = 0

            # iterate through time steps
            for t_step in AggFlex_res['AggFlex_Ulbig'][device]['POS']:

                flex_plan = AggFlex_res['AggFlex_Ulbig'][device]['POS'][t_step]
                if blocks == 4:
                    flex_sum = sum(flex_plan[flex_interval[0] + (t_pos):flex_interval[0] + t_pos + 1])
                else:
                    flex_sum = sum(flex_plan[flex_interval[0]:flex_interval[1]])

                # maximal flexibilty check
                if flex_sum > max_flex_sum:
                    max_flex_sum = flex_sum
                    DevFlex['POS'] = flex_plan

            if max_flex_sum == 0:
                DevFlex['POS'] = np.zeros((96))
            elif blocks == 4 and max_flex_sum != 0:
                t_pos = t_pos + 1

            AggFlex['Ulbig_' + str(device)] = DevFlex

# -----------------------------------------------------------------------------
    # FlexOffer
    if FlexOffer_Agg:
        for device in AggFlex_res['AggFlex_FlexOffer']:

            DevFlex = {}
            max_flex_sum = 0

            # iterate through time steps
            for t_step in AggFlex_res['AggFlex_FlexOffer'][device]['NEG']:

                flex_plan = AggFlex_res['AggFlex_FlexOffer'][device]['NEG'][t_step]
                flex_sum = sum(flex_plan[flex_interval[0]:flex_interval[1]])

                # maximal flexibility check
                if flex_sum < max_flex_sum:
                    max_flex_sum = flex_sum
                    DevFlex['NEG'] = flex_plan

            if max_flex_sum == 0:
                DevFlex['NEG'] = np.zeros((96))

            DevFlex['POS'] = np.zeros((96))

            AggFlex['FlexOffer_' + str(device)] = DevFlex


# -----------------------------------------------------------------------------
    # DFO
    if DFO_Agg:
        for device in AggFlex_res['AggFlex_DFO']:

            # if device != 'ev':
            DevFlex = {}
            max_flex_sum = 0

            # iterate through time steps
            for t_step in AggFlex_res['AggFlex_DFO'][device]['NEG']:

                for pow_step in AggFlex_res['AggFlex_DFO'][device]['NEG'][t_step]:

                    flex_plan = AggFlex_res['AggFlex_DFO'][device]['NEG'][t_step][pow_step]
                    flex_sum = sum(flex_plan[flex_interval[0]:flex_interval[1]])

                    # maximal flexibility check
                    if flex_sum < max_flex_sum:
                        max_flex_sum = flex_sum
                        DevFlex['NEG'] = flex_plan

            if max_flex_sum == 0:
                DevFlex['NEG'] = np.zeros((96))

            DevFlex['POS'] = np.zeros((96))

            AggFlex['DFO_' + str(device)] = DevFlex

    return AggFlex
