import numpy as np
import json
import time
import pandas as pd

# Save flexibility calculation results


# Save OpenTUM flexibility results
def save_OpenTUM_res(OpenTUM_res, OpenTUM_save):
    if OpenTUM_save:
        # transform DataFrames into dicts to be compatible with JSON
        OpenTUM_devices = OpenTUM_res['devices']
        OpenTUM_flex = OpenTUM_res['flexopts']
        OpenTUM_flex = {key: OpenTUM_flex[key].to_dict() for key in OpenTUM_flex.keys()}
        OpenTUM_res_dict = {'OpenTUM_devices': OpenTUM_devices,
                            'OpenTUM_flex': OpenTUM_flex}

        # Saving
        timestr = time.strftime("%Y%m%d-%H%M%S")
        save_file = open("results/JSON/OpenTUM_res_" + timestr + ".json", "w")
        json.dump(OpenTUM_res_dict, save_file, indent=2)
        save_file.close()


# https://www.programmersought.com/article/18271066028/
class NumpyEncoder(json.JSONEncoder):
    """ Special json encoder for numpy types """
    def default(self, obj):
        if isinstance(obj, (np.int_, np.intc, np.intp, np.int8,
                            np.int16, np.int32, np.int64, np.uint8,
                            np.uint16, np.uint32, np.uint64)):
            return int(obj)
        elif isinstance(obj, (np.float_, np.float16, np.float32,
                              np.float64)):
            return float(obj)
        elif isinstance(obj, (np.ndarray,)):
            return obj.tolist()
        return json.JSONEncoder.default(self, obj)


# Save Ulbig flexibility results
def save_Ulbig_res(Ulbig_res, Ulbig_save):
    if Ulbig_save:
        # Saving
        timestr = time.strftime("%Y%m%d-%H%M%S")
        save_file = open("results/JSON/Ulbig_res_" + timestr + ".json", "w")
        json.dump(Ulbig_res, save_file, indent=2, cls=NumpyEncoder)
        save_file.close()


# Save FlexOffer flexibility results
def save_FlexOffer_res(FlexOffer_res, FlexOffer_save, DFO_calc):
    if FlexOffer_save:
        # Converting DataFrames into dicts
        if DFO_calc:
            for device in FlexOffer_res:
                for DFO in FlexOffer_res[device]['DFOs']:
                    for t_step in FlexOffer_res[device]['DFOs'][DFO].keys():
                        FlexOffer_res[device]['DFOs'][DFO][t_step] = FlexOffer_res[device]['DFOs'][DFO][t_step].to_dict()

        # Saving
        timestr = time.strftime("%Y%m%d-%H%M%S")
        save_file = open("results/JSON/FlexOffer_res_" + timestr + ".json", "w")
        json.dump(FlexOffer_res, save_file, indent=2)
        save_file.close()

        # Converting dicts into DataFrames
        if DFO_calc:
            for device in FlexOffer_res:
                for DFO in FlexOffer_res[device]['DFOs']:
                    for t_step in FlexOffer_res[device]['DFOs'][DFO].keys():
                        FlexOffer_res[device]['DFOs'][DFO][t_step] = pd.DataFrame(
                            FlexOffer_res[device]['DFOs'][DFO][t_step])
