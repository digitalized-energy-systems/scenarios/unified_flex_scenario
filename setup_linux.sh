#!/bin/bash
# make sure this is not run as root
if [ "$EUID" == 0 ]; then
  echo "[Error] Don't run this as root"
  exit
fi

# exit if a command fails
set -e

# init submodules
git submodule update --init

# install glpk, pip, venv and requirements for PyGObject
sudo apt update
sudo apt install -y glpk-utils libglpk-dev python3-pip python3-venv
sudo apt install -y libgirepository1.0-dev gcc libcairo2-dev pkg-config python3-dev gir1.2-gtk-3.0

# create and activate a virtual environment
python3 -m venv ufs-venv
source ufs-venv/bin/activate

# install freqmatchmarket and its dependencies
cd aggregation/freqmatch-market
git submodule update --init
pip install wheel
pip install -r mango/mango/requirements.txt
pip install -e mango/mango
pip install -r requirements.txt
pip install -e .
sudo apt install -y mosquitto
sudo service mosquitto start

# make opentumflex path accessible, install requirements for this project
cd ../..
pip install -e flex_models/OpenTUMFlex_ufs
pip install -r requirements.txt

# gurobi - not usable without license
# gurobi with conda:
# conda config --add channels https://conda.anaconda.org/gurobi
# conda install -y gurobi
# gurobi with pip:
# python -m pip install gurobipy
