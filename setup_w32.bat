:: Init submodules
git submodule update --init
cd aggregation\freqmatch-market
git submodule update --init

:: Create virtual environment
call conda create --name ufs-venv python=3.8
call conda activate ufs-venv

:: Install GUI 4 freqmatch-market
call conda install -c conda-forge pygobject
call conda install -c conda-forge gtk3

:: Freqmatch-market requirements & and installations
pip install -r mango/mango/requirements.txt
pip install -e mango/mango
pip install -r requirements.txt
pip install -e .

cd ..\..
mkdir install_mqtt
cd install_mqtt
powershell wget https://mosquitto.org/files/binary/win32/mosquitto-2.0.12-install-windows-x86.exe -OutFile mosquitto.exe
powershell Start-Process .\mosquitto.exe -NoNewWindow -Wait
sc.exe start "mosquitto"

:: Install OpenTUMFlex requirements
cd ..
pip install -e flex_models\OpenTUMFlex_ufs
pip install -r requirements.txt

:: Path to visualisation
pip install -e visualisation

:: Install GLPK solver
call conda install -c conda-forge glpk

:: Install Gurobi & add licencse
::call conda config --add channels http://conda.anaconda.org/gurobi
::call conda install gurobi
::grbgetkey *Your license*

