import numpy as np
import matplotlib.pyplot as plt
import matplotlib.backends.backend_pdf
import platform
import os
import subprocess


# Plot of chosen and aggregated product in Flex2Agg res. FreqMatch
def Flex_Agg_visu(product_info, Agg_visu, Flex_init, Flex_final, OpenTUM_res):

    #List of market types
    market_list = ["SRL", "MRL", "DAY_AHEAD", "INTRADAY_AUCTION", "INTRADAY_TRADE"]    
    
    # Product info
    # day = product_info['day']
    interval = product_info['interval']
    market = market_list[product_info['Market_type']]
    blocks = product_info['blocks']
    positive = product_info['positive']
    flex_interval = np.add(np.array([0, blocks]), blocks * interval)

    # Color list
    c_list = ['#000000', '#EBA0E2', '#F5EC79', '#79D5F5', '#338D1B', '#FEB53C', '#C1C1C1',
              '#726570', '#D250C2', '#EFFE01', '#1AA2D1', '#65FE3C', '#CE580C', '#E6E5E6']

    # Label list
    label_list = ['Photovoltaikanlage', 'Blockheizkraftwerk', 'Wärmepumpe',
                  'Elektroauto', 'Batteriespeicher']

    # Label list for mixed aggregation
    # label_list = ['Photovoltaikanlage OTF', 'Blockheizkraftwerk OTF', 'Wärmepumpe DFO',
    #               'Elektroauto DFO', 'Batteriespeicher OTF']

    if Agg_visu:

        # Open pdf, define plot params
        agg_path = os.path.join("visualisation", "plots", "plotsAggregation.pdf")
        pdf = matplotlib.backends.backend_pdf.PdfPages(agg_path)
        width = 1
        Flex_print = {}

        # Calculate time line if product = positive, calculate ymin & ymax for plot
        if positive:
            for device in Flex_init:
                Flex_pos = np.array(Flex_init[device]['POS'][flex_interval[0]:flex_interval[1]]
                                    - Flex_final[device + '_final']['POS'][flex_interval[0]:flex_interval[1]])
                if round(sum(Flex_pos), 6) > 0:
                    Flex_print[device] = Flex_pos

            # Additional infos for mixed aggregation.
                # For activation, uncomment commented sections and change 'Flex_print' to 'Flex_print0' above
            # keys = list(Flex_print0.keys())
            # Flex_print = {'hp': Flex_print0[keys[3]], 'ev': Flex_print0[keys[4]], 'bhkw': Flex_print0[keys[0]],
            #      'bat': Flex_print0[keys[1]], 'pv': Flex_print0[keys[2]]}

            y_max = 0
            for i in range(blocks):
                y = 0
                for dev in Flex_print:
                    y = y + Flex_print[dev][i]
                if y > y_max:
                    y_max = y

            y_max = 1.3 * y_max
            y_min = -0.15 * y_max

        # Calculate time line if product = negative, calculate ymin & ymax for plot
        else:
            for device in Flex_init:
                Flex_neg = np.array(Flex_init[device]['NEG'][flex_interval[0]:flex_interval[1]]
                                    - Flex_final[device + '_final']['NEG'][flex_interval[0]:flex_interval[1]])

                if round(sum(Flex_neg), 6) < 0:
                    Flex_print[device] = Flex_neg

            # Additional infos for mixed aggregation.
                # For activation, uncomment commented sections and change 'Flex_print' to 'Flex_print0' above
            # keys = list(Flex_print0.keys())
            # Flex_print = {'hp': Flex_print0[keys[2]], 'ev': Flex_print0[keys[3]], 'bhkw': Flex_print0[keys[0]],
            #       'bat': Flex_print0[keys[1]]}

            y_min = 0
            for i in range(blocks):
                y = 0
                for dev in Flex_print:
                    y = y + Flex_print[dev][i]
                if y < y_min:
                    y_min = y

            y_min = 1.3 * y_min
            y_max = -0.15 * y_min

        # More plot parameters & open figure
        t_steps = np.array(list(range(blocks))) + 0.5
        y_label = 'Leistung in kW (EZS)'
        plt.figure(figsize=(15, 5))

        # Iterate through product parts of devices and bar plot them one over another
        for i, device in enumerate(Flex_print):

            if device[-3:] == '_hp':
                lab = label_list[2]
                col = c_list[1]
            elif device[-3:] == '_pv':
                lab = label_list[0]
                col = c_list[2]
            elif device[-4:] == '_chp':
                lab = label_list[1]
                col = c_list[0]
            elif device[-3:] == '_ev':
                lab = label_list[3]
                col = c_list[3]
            elif device[-4:] == '_bat':
                lab = label_list[4]
                col = c_list[4]

            # Additional infos for mixed aggregation.
                # For activation, uncomment section and and comment section above
            # if device == 'hp':
            #     lab = label_list[2]
            #     col = c_list[1]
            # elif device == 'pv':
            #     lab = label_list[0]
            #     col = c_list[2]
            # elif device == 'bhkw':
            #     lab = label_list[1]
            #     col = c_list[0]
            # elif device == 'ev':
            #     lab = label_list[3]
            #     col = c_list[3]
            # elif device == 'bat':
            #     lab = label_list[4]
            #     col = c_list[4]

            Flex_print_sum = 0

            if i == 0:
                plt.bar(t_steps, Flex_print[device], width, color=col, label=lab)

            else:
                for j in range(i):
                    Flex_print_sum = Flex_print_sum + Flex_print[list(Flex_print.keys())[j]]
                plt.bar(t_steps, Flex_print[device], width, bottom=Flex_print_sum, color=col, label=lab)

        plt.legend(loc='lower center', ncol=4, fontsize=14)
        plt.title('Aggregated Flexibility market_type:' + str(market))
        plt.ylim([y_min, y_max])
        plt.ylabel(y_label, fontsize=12)
        plt.xlabel('Zeitschritte à 15 Minuten', fontsize=12)
        plt.grid(which='both', color='0.5', linestyle='-', linewidth=0.5)
        pdf.savefig()
        pdf.close()
        if platform.system() == 'Darwin':  # macOS
            subprocess.call(('open', agg_path))
        elif platform.system() == 'Windows':  # Windows
            os.startfile(agg_path)
        else:  # linux variants
            os.system('/usr/bin/xdg-open %s 2>/dev/null' % agg_path)
        # plt.savefig('visualisation/plots/Agg_Flex.png', dpi=300)

    else:
        Agg_visu = 'Visualisation of flexibility aggregation is turned off'

    return Agg_visu
