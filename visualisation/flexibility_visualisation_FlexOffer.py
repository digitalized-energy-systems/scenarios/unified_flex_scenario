import os
import platform
import subprocess

import matplotlib.backends.backend_pdf
import matplotlib.pyplot as plt
import numpy as np

from visualisation.visualisation_config import normalised_plots_FlexOffer, \
    PLOT_TITLE, LABEL_FONT_SIZE, TICKS_FONT_SIZE, \
    LABEL_STRENGTH, STORE_SINGLE_PLOTS

PLOT_TITLE_FO = PLOT_TITLE


# Visualisation of FlexOffer results


def FlexOffer_flex_visu(OpenTUM_res, FlexOffer_res, FlexOffer_visu, DFO_visu):
    t_fac = OpenTUM_res['time_data']['t_inval'] / 60
    t_max = int(24 / t_fac)
    # define dictionaries for colors+linestyles and parameters for plots
    colors = {
        "es": "black",
        "ls": "hotpink",
        "le": "limegreen"
    }
    linestyles = {
        "es": "--",
        "ls": "-.",
        "le": "-"
    }
    linewidths = {
        "all": 3
    }
    tolerance_visibility = 0.15
    x_max = t_max - 1
    x_max_alter = 35
    x_ticks_count = 20
    x_ticks_count_alter = 8

    # ---------------------FlexOffers---------------------
    # FlexOffer visualisation
    if FlexOffer_visu:
        # Open PDF to save figures
        filepath_fo = os.path.join("visualisation", "plots",
                                   "plotsFlexOffer.pdf")
        pdf = matplotlib.backends.backend_pdf.PdfPages(filepath_fo)
        for device in FlexOffer_res:
            width = 1
            es_3d = None
            ls_3d = None
            if device == "ev":
                es_3d = FlexOffer_res[device]['FlexOffers']['FlexOffer_0'][
                    't_es']
                ls_3d = FlexOffer_res[device]['FlexOffers']['FlexOffer_0'][
                    't_ls']
            flex_neg = twod_plots(device, FlexOffer_res[device]['FlexOffers'],
                                  t_fac, t_max, width, tolerance_visibility,
                                  colors, linestyles, linewidths, x_max, x_ticks_count,
                                  pdf, es_3d, ls_3d, OpenTUM_res,
                                  STORE_SINGLE_PLOTS,
                                  dfo=False)
            if device == "ev":
                threed_flex = threed_plot_FO_data(OpenTUM_res, flex_neg, t_fac,
                                                  t_max, True, es_3d, ls_3d)
            elif device == "hp":
                threed_flex = threed_plot_FO_data(OpenTUM_res, flex_neg, t_fac,
                                                  t_max)
            else:
                print("device neither ev nor hp")
                threed_flex = None
            threed_plot(threed_flex, device, t_fac, t_max, x_max,
                        x_ticks_count, pdf, STORE_SINGLE_PLOTS)
        pdf.close()
        if platform.system() == 'Darwin':  # macOS
            subprocess.call(('open', filepath_fo))
        elif platform.system() == 'Windows':  # Windows
            os.startfile(filepath_fo)
        else:  # linux variants
            os.system('/usr/bin/xdg-open %s 2>/dev/null' % filepath_fo)

    # ---------------------Dependency-based FlexOffers---------------------
    # DFO visualisation
    if DFO_visu:
        # Open PDF to save figures
        filepath_dfo = os.path.join("visualisation", "plots", "plotsDFOs.pdf")
        pdf = matplotlib.backends.backend_pdf.PdfPages(filepath_dfo)
        width = 1
        alternative = True  # alternative EV plot (2x2 graphs)
        for device in FlexOffer_res:
            # Call number of slices per DFO and per timestep
            pow_nr = len(FlexOffer_res[device]['DFOs']['DFO_0'][0])

            # Plot optimal operation plan with possible deviations
            if alternative and device == 'ev':

                fig, axs = plt.subplots(nrows=2, ncols=2, figsize=(15, 8),
                                        sharey=True, sharex=True)

                for ax in axs.flat:
                    example_plot(ax)

                fig.subplots_adjust(hspace=0.15)
                if PLOT_TITLE_FO:
                    fig.suptitle('DFO_' + device)

            # Iterate through slices
            for variable_pow in range(pow_nr):
                if variable_pow > 0:
                    # Definition of variables
                    fac = int(24 / t_fac)
                    DFO_min_plan = np.zeros(fac)
                    DFO_max_plan = np.zeros(fac)
                    DFO_flex_neg = np.zeros(fac)
                    DFO_flex_pos = np.zeros(fac)
                    for index, DFO in enumerate(FlexOffer_res[device]['DFOs']):
                        # additional variables for alternative EV plot
                        if alternative and device == 'ev':
                            index = 0
                            DFO = 'DFO_0'
                        # Assignment of earliest start (es), latest start (ls) time and latest end (le) time variable
                        t_es = FlexOffer_res[device]['FlexOffers'][
                            'FlexOffer_' + str(index)]['t_es']
                        t_ls = FlexOffer_res[device]['FlexOffers'][
                            'FlexOffer_' + str(index)]['t_ls']
                        t_le = FlexOffer_res[device]['FlexOffers'][
                                   'FlexOffer_' + str(index)]['t_ls'] + \
                               len(FlexOffer_res[device]['DFOs'][DFO])

                        # Draw earliest start (es), latest start (ls) time and latest end (le) lines for EV
                        if device == 'ev' and not alternative:
                            t_es = t_es if t_es != 0 else tolerance_visibility
                            t_ls = t_ls if t_ls != 0 else tolerance_visibility
                            es = axs[variable_pow - 1].axvline(x=t_es,
                                                               color=colors[
                                                                   "es"],
                                                               linestyle=
                                                               linestyles[
                                                                   "es"], 
                                                               linewidth=
                                                               linewidths["all"])
                            ls = axs[variable_pow - 1].axvline(x=t_ls,
                                                               color=colors[
                                                                   "ls"],
                                                               linestyle=
                                                               linestyles[
                                                                   "ls"], 
                                                               linewidth=
                                                               linewidths["all"])
                            le = axs[variable_pow - 1].axvline(x=t_le,
                                                               color=colors[
                                                                   "le"],
                                                               linestyle=
                                                               linestyles[
                                                                   "le"], 
                                                               linewidth=
                                                               linewidths["all"])

                        # Normalising parameters
                        norm_fac, y_scaling, y_labeling, plot_title = normalised_plots_FlexOffer \
                            (device, OpenTUM_res)

                        # Build possible flexibility plan out of DFO slices of EV
                        if device == 'ev':
                            DFO_ev = FlexOffer_res[device]['DFOs'][DFO]
                            for t_step in range(len(DFO_ev)):
                                DFO_max_plan[t_step + t_ls] = -DFO_ev[t_step][
                                    'v_min'][0] / t_fac / norm_fac
                                if t_step == 0:
                                    DFO_min_plan[t_step + t_ls] = -DFO_ev[
                                        t_step]['v_max'][
                                        variable_pow] / t_fac / norm_fac
                                elif (t_step + 1) == t_le - t_ls:
                                    DFO_min_plan[t_step + t_ls] = DFO_min_plan[
                                                                      t_step + t_ls - 1] \
                                                                  - \
                                                                  DFO_max_plan[
                                                                      t_step + t_ls - 1] + \
                                                                  DFO_max_plan[
                                                                      t_step + t_ls]
                                else:
                                    DFO_min_plan[t_step + t_ls] = -(
                                            DFO_ev[t_step + 1]['d'][
                                                variable_pow]
                                            - DFO_ev[t_step]['d'][
                                                variable_pow]) / t_fac / norm_fac

                                DFO_flex_neg[t_step + t_ls] = DFO_min_plan[
                                                                  t_step + t_ls] - \
                                                              DFO_max_plan[
                                                                  t_step + t_ls]
                                DFO_flex_pos[t_step + t_ls] = DFO_min_plan[
                                                                  t_step + t_ls] - \
                                                              DFO_min_plan[
                                                                  t_step + t_ls]

                        # Build possible flexibility plan out of DFO slices of HP
                        if device == 'hp':
                            DFO_hp = FlexOffer_res[device]['DFOs'][DFO]
                            for t_step in range(len(DFO_hp)):
                                DFO_max_plan[t_step + t_ls] = -DFO_hp[t_step][
                                    'v_min'][0] / t_fac / norm_fac
                                if t_step == 0:
                                    DFO_min_plan[t_step + t_ls] = -DFO_hp[
                                        t_step]['v_max'][
                                        variable_pow] / t_fac / norm_fac
                                elif (t_step + 1) == t_le - t_ls:
                                    DFO_min_plan[t_step + t_ls] = DFO_min_plan[
                                                                      t_step + t_ls - 1] \
                                                                  - \
                                                                  DFO_max_plan[
                                                                      t_step + t_ls - 1] + \
                                                                  DFO_max_plan[
                                                                      t_step + t_ls]
                                else:
                                    DFO_min_plan[t_step + t_ls] = -(
                                            DFO_hp[t_step + 1]['d'][
                                                variable_pow]
                                            - DFO_hp[t_step]['d'][
                                                variable_pow]) / t_fac / norm_fac
                                DFO_flex_neg[t_step + t_ls] = DFO_min_plan[
                                                                  t_step + t_ls] - \
                                                              DFO_max_plan[
                                                                  t_step + t_ls]
                                DFO_flex_pos[t_step + t_ls] = DFO_min_plan[
                                                                  t_step + t_ls] - \
                                                              DFO_min_plan[
                                                                  t_step + t_ls]
                    t_steps = np.arange(24 / t_fac) + 0.5

                    # DFO plot with 4x1 graphs
                    if not alternative:
                        if variable_pow == pow_nr - 1:
                            y_max = max(1.2 * max(DFO_max_plan),
                                        -0.2 * (min(DFO_flex_neg)))
                            y_min = min(1.8 * min(DFO_min_plan),
                                        -0.8 * (max(DFO_flex_pos)))
                        else:
                            y_max = max(1.2 * max(DFO_max_plan),
                                        -0.2 * (min(DFO_flex_neg)))
                            y_min = min(1.3 * min(DFO_min_plan),
                                        -0.3 * (max(DFO_flex_pos)))

                        maxi = axs[variable_pow - 1].bar(t_steps, DFO_min_plan,
                                                         width, color='0.8')
                        mini = axs[variable_pow - 1].bar(t_steps, DFO_max_plan,
                                                         width, color='0.4')
                        axs[variable_pow - 1].grid(which='both', color='0.5',
                                                   linestyle='-',
                                                   linewidth=0.5)
                        axs[variable_pow - 1].set_ylim([y_min, y_max])
                        axs[variable_pow - 1].set_xlim([0, t_max])
                        axs[variable_pow - 1].set_xticks(
                            np.linspace(0, x_max, x_ticks_count),
                            fontsize=TICKS_FONT_SIZE)
                        plt.yticks(fontsize=TICKS_FONT_SIZE)
                        # axs[variable_pow - 1].set_ylabel(
                        #     y_labeling, fontsize=LABEL_FONT_SIZE)
                        titles = ['(A)', '(B)', '(C)', '(D)']
                        axs[variable_pow - 1].title.set_text(
                            titles[variable_pow - 1])

                        if variable_pow == pow_nr - 1:
                            # axs[variable_pow - 1].set_xlabel(
                            #     'Time steps à 15 minutes',
                            #     fontsize=LABEL_FONT_SIZE)
                            if device == 'ev':
                                axs[variable_pow - 1].legend(
                                    [maxi, ls, mini, le, es],
                                    ['Maximum charging power',
                                     'Latest starting time',
                                     'Minimum charge power',
                                     'Latest end time',
                                     'Earliest start time'],
                                    ncol=3, loc='lower center',
                                    fontsize=LABEL_FONT_SIZE - 3)
                            if device == 'hp':
                                axs[variable_pow - 1].legend(
                                    [maxi, ls, mini, le, es],
                                    ['Maximum power consumption',
                                     'Latest starting time',
                                     'Minimal power consumption',
                                     'Latest end time',
                                     'Earliest start time'],
                                    ncol=3, loc='lower center',
                                    fontsize=LABEL_FONT_SIZE - 3)
                            fig.text(0.5, 0.06, 'Time steps à 15 minutes',
                                     ha='center',
                                     va='center', fontsize=LABEL_FONT_SIZE,
                                     fontweight=LABEL_STRENGTH)
                            fig.text(0.09, 0.5, y_labeling, ha='center',
                                     va='center', rotation='vertical',
                                     fontsize=LABEL_FONT_SIZE,
                                     fontweight=LABEL_STRENGTH)
                            plt.tight_layout()
                            pdf.savefig()
                            if STORE_SINGLE_PLOTS is True:
                                plt.savefig(
                                    'visualisation/plots/DFO4x1_' + str(
                                        device) + '.png',
                                    dpi=300)

                    # DFO plot HP only for last flex combination
                    if device == 'hp' and variable_pow == pow_nr - 1:
                        y_max = max(1.2 * max(DFO_max_plan),
                                    -0.2 * (min(DFO_flex_neg)))
                        y_min = min(1.8 * min(DFO_min_plan),
                                    -0.8 * (max(DFO_flex_pos)))
                        fig, axs_hp = plt.subplots(1, 1, figsize=(15, 5))
                        maxi = axs_hp.bar(t_steps, DFO_min_plan, width,
                                          color='0.8')
                        mini = axs_hp.bar(t_steps, DFO_max_plan, width,
                                          color='0.4')
                        es = axs_hp.axvline(x=t_es + 0.15, color=colors["es"],
                                            linestyle=linestyles["es"],
                                            linewidth=linewidths["all"])
                        ls = axs_hp.axvline(x=t_ls + 0.15, color=colors["ls"],
                                            linestyle=linestyles["ls"],
                                            linewidth=linewidths["all"])
                        le = axs_hp.axvline(x=t_le - 0.15, color=colors["le"],
                                            linestyle=linestyles["le"],
                                            linewidth=linewidths["all"])
                        axs_hp.grid(which='both', color='0.5', linestyle='-',
                                    linewidth=0.5)
                        axs_hp.set_ylim([y_min, y_max])
                        axs_hp.set_xlim([0, t_max])
                        axs_hp.set_xticks(np.linspace(0, x_max, x_ticks_count),
                                          fontsize=TICKS_FONT_SIZE)
                        plt.yticks(fontsize=TICKS_FONT_SIZE)
                        plt.xticks(fontsize=TICKS_FONT_SIZE)
                        axs_hp.set_ylabel(y_labeling, fontsize=LABEL_FONT_SIZE,
                                          fontweight=LABEL_STRENGTH)
                        titles = ['DFO_single_' + str(device)]
                        if PLOT_TITLE_FO:
                            axs_hp.title.set_text(titles[0])
                        axs_hp.set_xlabel('Time steps à 15 minutes',
                                          fontsize=LABEL_FONT_SIZE,
                                          fontweight=LABEL_STRENGTH)
                        axs_hp.legend([maxi, ls, mini, le, es],
                                      ['Maximum power consumption',
                                       'Latest starting point',
                                       'Minimum power consumption',
                                       'Latest end time',
                                       'Earliest starting time'],
                                      loc='lower center', ncol=3,
                                      fontsize=LABEL_FONT_SIZE - 3)
                        plt.tight_layout()
                        pdf.savefig()
                        if STORE_SINGLE_PLOTS is True:
                            plt.savefig(
                                'visualisation/plots/DFO_' + str(
                                    device) + '.png',
                                dpi=300)

                    # 3D plot heat pump
                    if device == 'hp' and variable_pow == pow_nr - 1:
                        # 3D negative flex plot Heat Pump
                        threed_flex = threed_plot_FO_data(OpenTUM_res,
                                                          DFO_flex_neg, t_fac,
                                                          t_max, ev=False,
                                                          dfo=True)
                        threed_plot(threed_flex, device, t_fac, t_max, x_max,
                                    x_ticks_count, pdf, STORE_SINGLE_PLOTS)

                    # alternative EV DFO plot with 2x2 graphs
                    if device == 'ev' and alternative:
                        t_steps = np.arange(x_max_alter + 1) + 0.5
                        DFO_min_plan = DFO_min_plan[0:x_max_alter + 1]
                        DFO_max_plan = DFO_max_plan[0:x_max_alter + 1]
                        y_max = max(1.05 * max(DFO_max_plan),
                                    -0.05 * (min(DFO_flex_neg)))
                        y_min = min(1.05 * min(DFO_min_plan),
                                    -0.05 * (max(DFO_flex_pos)))
                        if variable_pow <= 2:
                            nrows = 0
                            ncols = nrows + int(variable_pow / 2)
                        if variable_pow > 2:
                            nrows = 1
                            ncols = nrows - 1 + int(variable_pow / 4) * 1
                        es = axs[nrows][ncols].axvline(x=t_es + 0.15,
                                                       color=colors["es"],
                                                       linestyle=linestyles["es"],
                                                       linewidth=linewidths["all"])
                        ls = axs[nrows][ncols].axvline(x=t_ls, color=colors["ls"],
                                                       linestyle=linestyles["ls"],
                                                       linewidth=linewidths["all"])
                        le = axs[nrows][ncols].axvline(x=t_le, color=colors["le"],
                                                       linestyle=linestyles["le"],
                                                       linewidth=linewidths["all"])
                        maxi = axs[nrows][ncols].bar(t_steps, DFO_min_plan,
                                                     width, color='0.8')
                        mini = axs[nrows][ncols].bar(t_steps, DFO_max_plan,
                                                     width, color='0.4')
                        axs[nrows][ncols].grid(which='both', color='0.5',
                                               linestyle='-', linewidth=0.5)
                        axs[nrows][ncols].set_ylim([y_min, y_max])
                        axs[nrows][ncols].set_xlim([0, x_max_alter + 1])
                        axs[nrows][ncols].set_xticks(
                            np.linspace(0, x_max_alter, x_ticks_count_alter),
                            fontsize=TICKS_FONT_SIZE)
                        # plt.yticks(fontsize=TICKS_FONT_SIZE)
                        # plt.xticks(fontsize=TICKS_FONT_SIZE)
                        # if ncols == 0:
                        #     axs[nrows][ncols].set_ylabel(
                        #         y_labeling, fontsize=LABEL_FONT_SIZE)
                        titles = ['(A)', '(B)', '(C)', '(D)']
                        axs[nrows][ncols].title.set_text(
                            titles[variable_pow - 1])
                        # if nrows == 1:
                        #     axs[nrows][ncols].set_xlabel(
                        #         'Time steps à 15 minutes', fontsize=LABEL_FONT_SIZE)
                        if variable_pow == pow_nr - 1:
                            axs[nrows][ncols].legend([maxi, ls, mini, le, es],
                                                     ['Maximum charging power',
                                                      'Latest starting point',
                                                      'Minimum charging power',
                                                      'Latest end point',
                                                      'Earliest starting point'],
                                                     loc=(-1.205, -0.5),
                                                     ncol=3,
                                                     fontsize=LABEL_FONT_SIZE - 3)
                            fig.text(0.08, 0.5, y_labeling, ha='center',
                                     va='center', rotation='vertical',
                                     fontsize=LABEL_FONT_SIZE,
                                     fontweight=LABEL_STRENGTH)
                            fig.text(0.5, 0.055, 'Time steps à 15 minutes',
                                     ha='center',
                                     va='center', fontsize=LABEL_FONT_SIZE,
                                     fontweight=LABEL_STRENGTH)
                            # plt.tight_layout()
                            fig.subplots_adjust(bottom=0.105)
                            pdf.savefig(bbox_inches="tight")
                            if STORE_SINGLE_PLOTS is True:
                                plt.savefig(
                                    'visualisation/plots/DFO2x2_' + str(
                                        device) + '.png',
                                    dpi=300, bbox_inches="tight")
                        # 3D negative flexibilty plot EV
                        if variable_pow == pow_nr - 1:
                            t_es = t_es + 1
                            t_ls = t_ls + 1
                            threed_flex = threed_plot_FO_data(OpenTUM_res,
                                                              DFO_flex_neg,
                                                              t_fac, t_max,
                                                              True, t_es, t_ls)
                            threed_plot(threed_flex, device, t_fac, t_max,
                                        x_max, x_ticks_count, pdf,
                                        STORE_SINGLE_PLOTS)
        pdf.close()
        if platform.system() == 'Darwin':  # macOS
            subprocess.call(('open', filepath_dfo))
        elif platform.system() == 'Windows':  # Windows
            os.startfile(filepath_dfo)
        else:  # linux variants
            os.system('/usr/bin/xdg-open %s 2>/dev/null' % filepath_dfo)


def twod_plots(device, FlexOffers, t_fac, t_max, width, tolerance_visibility,
               colors, linestyles, linewidths, x_max, x_ticks_count, pdf, es_3d, ls_3d,
               OpenTUM_res, STORE_SINGLE_PLOTS,
               dfo):
    fac = int(t_max)
    min_plan = np.zeros(fac)
    max_plan = np.zeros(fac)
    flex_neg = np.zeros(fac)
    flex_pos = np.zeros(fac)

    # Normalising parameters
    norm_fac, y_scaling, y_labeling, plot_title = normalised_plots_FlexOffer(
        device, OpenTUM_res)

    for FO in FlexOffers:
        # Assignment of earliest start and latest start time variable
        t_ls = FlexOffers[FO]['t_ls']
        for t_step in range(len(FlexOffers[FO]['p'])):
            # Assignment of flexibility plans
            max_plan[t_step + t_ls] = -FlexOffers[FO]['p'][t_step][
                0] / t_fac / norm_fac
            min_plan[t_step + t_ls] = -FlexOffers[FO]['p'][t_step][
                1] / t_fac / norm_fac
            flex_neg[t_step + t_ls] = (-FlexOffers[FO]['p'][t_step][1] +
                                       FlexOffers[FO]['p'][t_step][
                                           0]) / t_fac / norm_fac
            flex_pos[t_step + t_ls] = (-FlexOffers[FO]['p'][t_step][1] +
                                       FlexOffers[FO]['p'][t_step][
                                           1]) / t_fac / norm_fac
    t_steps = np.arange(24 / t_fac) + 0.5
    y_label = y_labeling

    # FlexOffer plot
    y_max = max(1.3 * max(max_plan), -0.3 * (min(flex_neg)))
    y_min = min(1.4 * min(min_plan), -0.4 * (max(flex_pos)))
    plt.figure(figsize=(15, 5))

    # Get availability duration
    if device == 'ev':
        ev = True
    else:
        ev = False

    plt.grid(which='both', color='0.5', linestyle='-', linewidth=0.5)
    # Draw lines for earliest start (es), latest start (ls) and latest end (le) time

    for FO in FlexOffers:
        t_es = FlexOffers[FO]['t_es'] if FlexOffers[FO][
                                             't_es'] != 0 else tolerance_visibility
        t_ls = FlexOffers[FO]['t_ls'] if FlexOffers[FO][
                                             't_ls'] != 0 else tolerance_visibility
        t_le = t_ls + len(FlexOffers[FO]['p']) if t_ls + len(
            FlexOffers[FO]['p']) <= t_max - 1 else t_max - tolerance_visibility
        es = plt.axvline(x=t_es, color=colors["es"],
                         linestyle=linestyles["es"],
                         linewidth=linewidths["all"])
        ls = plt.axvline(x=t_ls, color=colors["ls"],
                         linestyle=linestyles["ls"],
                         linewidth=linewidths["all"])
        le = plt.axvline(x=t_le, color=colors["le"],
                         linestyle=linestyles["le"],
                         linewidth=linewidths["all"])
    maxi = plt.bar(t_steps, min_plan, width, color='0.8')
    mini = plt.bar(t_steps, max_plan, width, color='0.4')

    if ev:
        plt.legend([maxi, ls, mini, le, es], [
            'Maximum charging power',
            'Latest starting time',
            'Minimum charging power',
            'Latest end time',
            'Earliest starting point'],
                   loc='lower center', ncol=3, fontsize=LABEL_FONT_SIZE - 1.5)
    else:
        plt.legend([maxi, ls, mini, le, es], [
            'Maximum power consumption',
            'Latest starting time',
            'Minimum power consumption',
            'Latest end time',
            'Earliest starting point'],
                   loc='lower center', ncol=3, fontsize=LABEL_FONT_SIZE - 3)

    if PLOT_TITLE is True:
        plt.title('FlexOffer ' + plot_title, fontsize=LABEL_FONT_SIZE)
    plt.ylim([y_min, y_max])
    plt.ylabel(y_label, fontsize=LABEL_FONT_SIZE, fontweight=LABEL_STRENGTH)
    plt.xlim([0, t_max])
    plt.xticks(np.linspace(0, x_max, x_ticks_count), fontsize=TICKS_FONT_SIZE)
    plt.yticks(fontsize=TICKS_FONT_SIZE)
    plt.xlabel('Time steps à 15 minutes', fontsize=LABEL_FONT_SIZE,
               fontweight=LABEL_STRENGTH)
    plt.tight_layout()
    pdf.savefig()
    if STORE_SINGLE_PLOTS is True:
        plt.savefig(
            'visualisation/plots/FO_' + str(device) + '.png',
            dpi=300)

    # Simple flex plot
    y_max = max(1.3 * max(flex_pos), -0.3 * (min(flex_neg)))
    y_min = min(1.4 * min(flex_neg), -0.4 * (max(flex_pos)))
    plt.figure(figsize=(15, 5))

    pos = plt.bar(t_steps, flex_pos, width, color='0.8')
    neg = plt.bar(t_steps, flex_neg, width, color='0.4')
    plt.legend((pos[0], neg[0]),
               ('Positive flexibility', 'Negative flexibility'),
               loc='lower center', ncol=2, fontsize=LABEL_FONT_SIZE)

    if PLOT_TITLE is True:
        plt.title('FlexOffer flexibility ' + plot_title,
                  fontsize=LABEL_FONT_SIZE)
    plt.ylim([y_min, y_max])
    plt.ylabel(y_label, fontsize=LABEL_FONT_SIZE, fontweight=LABEL_STRENGTH)
    plt.xlim([0, t_max])
    plt.xticks(np.linspace(0, x_max, x_ticks_count), fontsize=TICKS_FONT_SIZE)
    plt.yticks(fontsize=TICKS_FONT_SIZE)
    plt.xlabel('Time steps à 15 minutes', fontsize=LABEL_FONT_SIZE,
               fontweight=LABEL_STRENGTH)
    plt.grid(which='both', color='0.5', linestyle='-', linewidth=0.5)
    plt.tight_layout()
    pdf.savefig()
    # if STORE_SINGLE_PLOTS is True:
    #     plt.savefig(
    #         'visualisation/plots/FO_flex_' + str(device) + '.png',
    #         dpi=300)
    return flex_neg


def threed_plot(threed_flex, device, t_fac, t_max, x_max, x_ticks_count, pdf,
                STORE_SINGLE_PLOTS):
    if np.mean(threed_flex['dz_neg']) != 0:
        fig, (flexneg3d) = plt.subplots(1, 1, figsize=(15, 15),
                                        subplot_kw=dict(projection='3d'))
        if PLOT_TITLE_FO:
            flexneg3d.set_title('DFO 3d negative flexibility ' + device,
                                fontsize=LABEL_FONT_SIZE)
        x3_neg = threed_flex['x3_neg']
        y3_neg = threed_flex['y3_neg']
        z3_neg = threed_flex['z3_neg']
        dx_neg = threed_flex['dx_neg']
        dy_neg = threed_flex['dy_neg']
        dz_neg = threed_flex['dz_neg']
        zo_neg = threed_flex['zo_neg']
        bars = np.empty(x3_neg.shape, dtype=object)
        for i, (x3, y3, z3, dx, dy, dz, o) in enumerate(
                ravzip(x3_neg, y3_neg, z3_neg, dx_neg, dy_neg, dz_neg,
                       zo_neg)):
            j, k = divmod(i, t_max)
            if dz != 0:
                bars[j, k] = pl = flexneg3d.bar3d(x3, y3, z3, dx, dy, dz,
                                                  color='0.4')
            else:
                bars[j, k] = pl = flexneg3d.bar3d(x3, y3, z3, dx, dy, dz,
                                                  color='1', alpha=0)
            pl._sort_zpos = o
        flexneg3d.set_xlim(0, t_max)
        flexneg3d.set_xticks(np.linspace(0, x_max, x_ticks_count))
        flexneg3d.set_xlabel('Time steps à 15 minutes', fontsize=14)
        flexneg3d.set_ylabel(
            'Avaibable flexibility duration in time steps of 15 minutes each',
            fontsize=14)
        flexneg3d.set_zlabel('Power in kW (EZS)', fontsize=14, labelpad=10)
        flexneg3d.zaxis.set_tick_params(direction='out', pad=8)
        plt.tight_layout()
        pdf.savefig()
        # if STORE_SINGLE_PLOTS is True:
        #     plt.savefig(
        #         'visualisation/plots/DFO_3DNeg_' + str(device) + '.png',
        #         dpi=300)


# Data preparation for 3D plot
def threed_plot_FO_data(OpenTUM_res, flex_neg, t_fac, t_max, ev=False,
                        es_3d=None, ls_3d=None, dfo=False):
    # Time factor for conversion calculations
    t_fac = OpenTUM_res['time_data']['t_inval'] / 60
    if ev:
        # Data correction
        for ind, i in enumerate(flex_neg):
            if abs(i) < 0.001:
                flex_neg[ind] = 0
        # Calculation of depth of negative 3D plot (availability duration)
        flex_length_neg = np.zeros((int(24 / t_fac)))
        flex_length_neg[es_3d:ls_3d + 1] = len(
            np.array(np.nonzero(flex_neg)[0]))
    elif not ev and not dfo:
        flex_length_neg = np.ones((int(24 / t_fac)))
    else:
        flex_length_neg = np.zeros((int(24 / t_fac)))
        flex_length_neg[0] = len(flex_neg)
    # Minimum depth of negative 3D plot
    max_l_neg = abs(int(max(flex_length_neg) + 2))
    if max_l_neg < 10:
        max_l_neg = 10

    # Flexible power
    if not dfo:
        flex_pow_neg = flex_neg[np.nonzero(flex_neg)] if ev else flex_neg

    # Plot arrays negative
    x3_neg = np.zeros((max_l_neg, t_max))
    y3_neg = np.zeros((max_l_neg, t_max))
    z3_neg = np.zeros((max_l_neg, t_max))
    dx_neg = np.zeros((max_l_neg, t_max))
    dy_neg = np.zeros((max_l_neg, t_max))
    dz_neg = np.zeros((max_l_neg, t_max))

    # Fill negative plot arrays
    for t_step, lgth in enumerate(flex_length_neg):
        if lgth > 0:
            for flex_step in range(int(lgth)):
                x3_neg[flex_step][t_step] = t_step
                y3_neg[flex_step][t_step] = flex_step
                z3_neg[flex_step][t_step] = 0
                dx_neg[flex_step][t_step] = 1
                dy_neg[flex_step][t_step] = 1
                dz_neg[flex_step][t_step] = flex_neg[flex_step] if dfo else \
                    flex_pow_neg[flex_step if ev else t_step]
            for flex_step in range(int(lgth), max_l_neg):
                x3_neg[flex_step][t_step] = t_step
                y3_neg[flex_step][t_step] = flex_step
                z3_neg[flex_step][t_step] = 0
                dx_neg[flex_step][t_step] = 1
                dy_neg[flex_step][t_step] = 1
                dz_neg[flex_step][t_step] = 0
        if lgth == 0:
            for flex_step in range(max_l_neg):
                x3_neg[flex_step][t_step] = t_step
                y3_neg[flex_step][t_step] = flex_step
                z3_neg[flex_step][t_step] = 0
                dx_neg[flex_step][t_step] = 1
                dy_neg[flex_step][t_step] = 1
                dz_neg[flex_step][t_step] = 0

    # Fill array zo_neg for the right plot order of negative 3D plot
    zo_neg = np.zeros(x3_neg.shape)
    for row, num in enumerate(zo_neg):
        for col in range(len(num)):
            zo_neg[row][col] = -((row + 1) * len(num) - col) + (
                    len(zo_neg) * len(num))

    # Fill plot dict
    threed_data = {'x3_neg': x3_neg, 'y3_neg': y3_neg,
                   'z3_neg': z3_neg, 'dx_neg': dx_neg, 'dy_neg': dy_neg,
                   'dz_neg': dz_neg, 'zo_neg': zo_neg}

    return threed_data


# flatten and zip arrays
def ravzip(*itr):
    return zip(*map(np.ravel, itr))


def get_availability_duration(flex_pos, flex_neg, t_fac, ev=False,
                              es_3d=None, ls_3d=None, dfo=False):
    if ev:
        # Data correction
        for ind, i in enumerate(flex_neg):
            if abs(i) < 0.001:
                flex_neg[ind] = 0
        # Calculation of depth of negative 3D plot (availability duration)
        flex_length_neg = np.zeros((int(24 / t_fac)))
        flex_length_neg[es_3d:ls_3d + 1] = len(
            np.array(np.nonzero(flex_neg)[0]))
    elif not ev and not dfo:
        flex_length_neg = np.ones((int(24 / t_fac)))
    else:
        flex_length_neg = np.zeros((int(24 / t_fac)))
        flex_length_neg[0] = len(flex_neg)
    flex_length_pos = np.array([0 for i in range(len(flex_pos))])
    return flex_length_pos, flex_length_neg


# Unified font sizes for subplots
def example_plot(ax):
    ax.plot([1, 2])
    plt.setp(ax.get_xticklabels(), fontsize=TICKS_FONT_SIZE)
    plt.setp(ax.get_yticklabels(), fontsize=TICKS_FONT_SIZE)
