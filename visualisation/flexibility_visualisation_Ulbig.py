import os
import platform
import subprocess

import matplotlib.backends.backend_pdf
import matplotlib.pyplot as plt
import numpy as np

# Visualisation of Ulbig results
# There are 3 different types of graphs, that can be created:
# 1. Optimal operation plan with possible deviations (flexibility)
# 2. Possible deviations (flexibility)
# 3. 3D-plot of positive or negative possible deviations with availability duration
from visualisation.visualisation_config import normalised_plots_Ulbig, \
    color_availability, \
    COLOR, OPTI_PLAN_COLOR, \
    EV_AVAILABILITY_END_COLOR, EV_AVAILABILITY_BEGIN_COLOR, TICKS_FONT_SIZE, \
    LABEL_FONT_SIZE, PLOT_TITLE, STORE_SINGLE_PLOTS, LABEL_STRENGTH


def Ulbig_flex_visu(Ulbig_res, visu, OpenTUM_res):
    t_fac = OpenTUM_res['time_data']['t_inval'] / 60
    t_max = int(24 / t_fac)
    tolerance_visibility = 0.15
    x_max = t_max - 1
    x_ticks_count = 20

    if visu:
        # Open PDF to save figures
        filepath_fo = os.path.join("visualisation", "plots", "plotsUlbig.pdf")
        pdf = matplotlib.backends.backend_pdf.PdfPages(filepath_fo)
        plot_res = {}

        for idx, device in enumerate(Ulbig_res['ulbig_flex']):
            # Request of device type
            d_type = Ulbig_res['ulbig_flex'][device]['device_type']
            normal_fac, y_scaling, y_labeling, plot_title = normalised_plots_Ulbig \
                (device, OpenTUM_res, Ulbig_res, d_type)

            width = 1

            # -----------Generation devices-----------
            # Plots of generation devices
            if d_type == 'generator':

                # Assignment of variables
                opt_plan = Ulbig_res['ulbig_devices'][device][
                               'opt_plan'] / normal_fac
                min_plan = Ulbig_res['ulbig_flex'][device][
                               'u_gen_fea_min'] / normal_fac
                max_plan = Ulbig_res['ulbig_flex'][device][
                               'u_gen_fea_max'] / normal_fac
                flex_pos = Ulbig_res['ulbig_flex'][device][
                               'pi_gen_pos'] / normal_fac
                flex_neg = Ulbig_res['ulbig_flex'][device][
                               'pi_gen_neg'] / normal_fac
                t_steps = np.arange(len(opt_plan)) + 0.5
                y_label = y_labeling

                # Deviation from opti plan plot generation devices
                y_max = 1.1 * y_scaling
                y_min = -0.4 * y_max
                plt.figure(figsize=(15, 5))
                if COLOR is True:
                    avail_pos, avail_neg = get_availability_duration(flex_pos,
                                                                     flex_neg)
                    rgba_colors_pos, rgba_colors_neg = color_availability(
                        avail_pos, avail_neg, plt, unit=device,
                        model_type='ulbig')
                    maxi = plt.bar(t_steps, max_plan, width,
                                   color=rgba_colors_pos)
                    opti = plt.bar(t_steps, opt_plan, width,
                                   color=rgba_colors_neg)
                    plt.bar(t_steps, min_plan, width,
                            color=OPTI_PLAN_COLOR)
                    opti_line = plt.step(np.append(t_steps - 0.5, t_max),
                                         np.append(opt_plan, 0),
                                         linewidth=2, where='post', color='k')
                    plt.legend([opti_line[0]], ['Operation schedule'],
                               loc='lower right',
                               fontsize=LABEL_FONT_SIZE - 1)

                else:
                    maxi = plt.bar(t_steps, max_plan, width, color='0.8')
                    opti = plt.bar(t_steps, opt_plan, width, color='0.4')
                    plt.bar(t_steps, min_plan, width, color='1')
                    opti_line = plt.step(np.append(t_steps - 0.5, t_max),
                                         np.append(opt_plan, 0),
                                         linewidth=2, where='post', color='k')
                    plt.legend((maxi[0], opti[0], opti_line[0]),
                               ('Positive Flexibility',
                                'Negative Flexibility',
                                'Operation schedule'), loc='lower center',
                               ncol=3,
                               fontsize=LABEL_FONT_SIZE)
                plt.ylim([y_min, y_max])
                plt.ylabel(y_label, fontsize=LABEL_FONT_SIZE,
                           fontweight=LABEL_STRENGTH)
                plt.xlim([0, t_max])
                plt.xticks(np.linspace(0, x_max, x_ticks_count),
                           fontsize=TICKS_FONT_SIZE)
                plt.yticks(fontsize=TICKS_FONT_SIZE)
                plt.xlabel('Time steps à 15 minutes', fontsize=LABEL_FONT_SIZE,
                           fontweight=LABEL_STRENGTH)

                if PLOT_TITLE is True:
                    plt.title('Ulbig var ' + plot_title,
                              fontsize=LABEL_FONT_SIZE)
                plt.grid(which='both', color='0.5', linestyle='-',
                         linewidth=0.5)
                plt.tight_layout()
                pdf.savefig()

                # if STORE_SINGLE_PLOTS is True:
                #     plt.savefig(
                #         'visualisation/plots/Ulbig_Dev_' + str(
                #             device) + '.png',
                #         dpi=300)

                # Simple flex plot generation devices
                y_max = max(1.3 * max(flex_pos), -0.3 * (min(flex_neg)))
                y_min = min(1.4 * min(flex_neg), -0.4 * (max(flex_pos)))
                plt.figure(figsize=(15, 5))
                if COLOR is True:
                    avail_pos, avail_neg = get_availability_duration(flex_pos,
                                                                     flex_neg)

                    rgba_colors_pos, rgba_colors_neg = color_availability(
                        avail_pos, avail_neg, plt, unit=device,
                        model_type='ulbig')

                    plt.bar(t_steps, flex_pos, width, color=rgba_colors_pos)
                    plt.bar(t_steps, flex_neg, width, color=rgba_colors_neg)
                else:
                    pos = plt.bar(t_steps, flex_pos, width, color='0.8')
                    neg = plt.bar(t_steps, flex_neg, width, color='0.4')
                    plt.legend((pos[0], neg[0]), (
                        'Positive flexibility', 'Negative flexibility'),
                               loc='lower center', ncol=2,
                               fontsize=LABEL_FONT_SIZE)
                plt.ylim([y_min, y_max])
                plt.ylabel(y_label, fontsize=LABEL_FONT_SIZE,
                           fontweight=LABEL_STRENGTH)
                plt.xlim([0, t_max])
                plt.xticks(np.linspace(0, x_max, x_ticks_count),
                           fontsize=TICKS_FONT_SIZE)
                plt.yticks(fontsize=TICKS_FONT_SIZE)
                plt.xlabel('Time steps à 15 minutes', fontsize=LABEL_FONT_SIZE,
                           fontweight=LABEL_STRENGTH)
                if PLOT_TITLE is True:
                    plt.title('Ulbig flexibility ' + plot_title,
                              fontsize=LABEL_FONT_SIZE)
                plt.grid(which='both', color='0.5', linestyle='-',
                         linewidth=0.5)
                plt.tight_layout()
                pdf.savefig()

                # if STORE_SINGLE_PLOTS is True:
                #     plt.savefig(
                #         'visualisation/plots/Ulbig_Flex_' + str(
                #             device) + '.png',
                #         dpi=300)

                # 3D flex plots generation devices
                threed_flex_pos, threed_flex_neg = threed_plot_ulbig_data(
                    flex_pos, flex_neg, device, t_max)
                threedplot(threed_flex_pos, threed_flex_neg, device, x_max,
                           t_max, x_ticks_count, pdf)

            # -----------Load devices-----------
            # Plots of load devices
            elif d_type == 'load':

                # Assignment of variables
                opt_plan = -Ulbig_res['ulbig_devices'][device][
                    'opt_plan'] / normal_fac
                min_plan = -Ulbig_res['ulbig_flex'][device][
                    'u_load_fea_min'] / normal_fac
                max_plan = -Ulbig_res['ulbig_flex'][device][
                    'u_load_fea_max'] / normal_fac
                flex_pos = -Ulbig_res['ulbig_flex'][device][
                    'pi_load_neg'] / normal_fac
                flex_neg = -Ulbig_res['ulbig_flex'][device][
                    'pi_load_pos'] / normal_fac
                t_steps = np.arange(len(opt_plan)) + 0.5
                y_label = y_labeling

                # Availability evaluation for EV
                if device == 'ev':
                    aval = Ulbig_res['ulbig_devices']['ev']['ava']
                    aval[32] = 1.0
                    aval[95] = 1.0
                    aval_init = []
                    aval_end = []

                    for t_step, ava in enumerate(aval):
                        if t_step == 0:
                            aval_init.append(t_step)

                        elif ava == 1 and aval[t_step - 1] == 0:
                            aval_init.append(t_step)

                        if t_step == 95:
                            aval_end.append(t_step)

                        elif ava == 1 and aval[t_step + 1] == 0:
                            aval_end.append(t_step)

                    aval_init = np.array(aval_init)
                    aval_end = np.array(aval_end)

                # Deviation from opti plan plot load devices
                y_min = -1.4 * y_scaling
                y_max = -0.1 * y_min
                plt.figure(figsize=(15, 5))

                if COLOR is True:
                    avail_pos, avail_neg = get_availability_duration(flex_pos,
                                                                     flex_neg)

                    rgba_colors_pos, rgba_colors_neg = color_availability(
                        avail_pos, avail_neg, plt, unit=device,
                        model_type='ulbig')

                    maxi = plt.bar(t_steps, max_plan, width,
                                   color=rgba_colors_neg)
                    opti = plt.bar(t_steps, opt_plan, width,
                                   color=rgba_colors_pos)
                    plt.bar(t_steps, min_plan, width,
                            color=OPTI_PLAN_COLOR)

                    opti_line = plt.step(np.append(t_steps - 0.5, t_max),
                                         np.append(opt_plan, 0),
                                         linewidth=2, where='post', color='k')
                    plt.legend([opti_line[0]], ['Operation schedule'],
                               loc='lower right',
                               fontsize=LABEL_FONT_SIZE - 1)
                else:
                    maxi = plt.bar(t_steps, max_plan, width, color='0.4')
                    opti = plt.bar(t_steps, opt_plan, width, color='0.8')
                    plt.bar(t_steps, min_plan, width, color='1')
                    opti_line = plt.step(np.append(t_steps - 0.5, t_max),
                                         np.append(opt_plan, 0),
                                         linewidth=2, where='post', color='k')
                    plt.legend((opti[0], maxi[0], opti_line[0]),
                               ('Positive Flexibility',
                                'Negative Flexibility',
                                'Operation schedule'), loc='lower center',
                               ncol=3,
                               fontsize=14)

                # Plot of availability lines and legend for EV
                if device == 'ev':
                    if COLOR is True:
                        aval_init_1 = plt.axvline(
                            x=aval_init[0] + tolerance_visibility,
                            color=EV_AVAILABILITY_BEGIN_COLOR,
                            linestyle="dashed", linewidth=2.5)
                        aval_init_2 = plt.axvline(x=aval_init[1],
                                                  color=EV_AVAILABILITY_BEGIN_COLOR,
                                                  linestyle="dashed",
                                                  linewidth=2.5)

                        aval_end_1 = plt.axvline(x=aval_end[0] + 1,
                                                 color=EV_AVAILABILITY_END_COLOR,
                                                 linestyle="dashdot",
                                                 linewidth=2.5)
                        aval_end_2 = plt.axvline(
                            x=aval_end[1] + 1 - tolerance_visibility,
                            color=EV_AVAILABILITY_END_COLOR,
                            linestyle="dashdot", linewidth=2.5)
                        plt.legend(
                            [aval_init_2, aval_end_2,
                             opti_line[0]],
                            ['Start availabiltiy',
                             'End availabiltiy',
                             'Operation schedule'],
                            loc='lower center', ncol=3,
                            fontsize=LABEL_FONT_SIZE - 1)
                    else:
                        aval_init_1 = plt.axvline(
                            x=aval_init[0] + tolerance_visibility,
                            color='cyan',
                            linestyle="-.")
                        aval_init_2 = plt.axvline(x=aval_init[1], color='cyan',
                                                  linestyle="-.")

                        aval_end_1 = plt.axvline(x=aval_end[0] + 1,
                                                 color='orange',
                                                 linestyle="-.")
                        aval_end_2 = plt.axvline(
                            x=aval_end[1] + 1 - tolerance_visibility,
                            color='orange', linestyle="-.")
                        plt.legend(
                            [opti, aval_init_2, maxi, aval_end_2,
                             opti_line[0]],
                            ['Positive Flexibility', 'Start availabiltiy'
                                , 'Negative Flexibility',
                             'End availabiltiy',
                             'Operation schedule'],
                            loc='lower center', ncol=3,
                            fontsize=LABEL_FONT_SIZE)
                plt.ylim([y_min, y_max])
                plt.ylabel(y_label, fontsize=LABEL_FONT_SIZE, fontweight=LABEL_STRENGTH)
                plt.xlim([0, t_max])
                plt.xticks(np.linspace(0, x_max, x_ticks_count),
                           fontsize=TICKS_FONT_SIZE)
                plt.yticks(fontsize=TICKS_FONT_SIZE)
                plt.xlabel('Time steps à 15 minutes',
                           fontsize=LABEL_FONT_SIZE, fontweight=LABEL_STRENGTH)

                # if device != 'ev':
                if PLOT_TITLE is True:
                    plt.title('Ulbig var ' + plot_title,
                              fontsize=LABEL_FONT_SIZE)

                plt.grid(which='both', color='0.5', linestyle='-',
                         linewidth=0.5)
                plt.tight_layout()
                pdf.savefig()

                if STORE_SINGLE_PLOTS is True:
                    plt.savefig(
                        'visualisation/plots/Ulbig_Dev_' + str(
                            device) + '.png',
                        dpi=300)

                # Simple flex plot of load devices
                y_max = max(1.3 * max(flex_pos), -0.3 * (min(flex_neg)))
                y_min = min(1.5 * min(flex_neg), -0.5 * (max(flex_pos)))
                plt.figure(figsize=(15, 5))

                # Plot of availability lines and legend for EV
                if device == 'ev':
                    if COLOR is True:
                        aval_init_1 = plt.axvline(
                            x=aval_init[0] + tolerance_visibility,
                            color=EV_AVAILABILITY_BEGIN_COLOR,
                            linestyle="dashed", linewidth=2.5)
                        aval_init_2 = plt.axvline(x=aval_init[1],
                                                  color=EV_AVAILABILITY_BEGIN_COLOR,
                                                  linestyle="dashed",
                                                  linewidth=2.5)

                        aval_end_1 = plt.axvline(x=aval_end[0] + 1,
                                                 color=EV_AVAILABILITY_END_COLOR,
                                                 linestyle="dashdot",
                                                 linewidth=2.5)
                        aval_end_2 = plt.axvline(
                            x=aval_end[1] + 1 - tolerance_visibility,
                            color=EV_AVAILABILITY_END_COLOR,
                            linestyle="dashdot", linewidth=2.5)
                        plt.legend([aval_init_2, aval_end_2],
                                   ['Start availability', 'End availability'],
                                   loc='lower center', ncol=2,
                                   fontsize=LABEL_FONT_SIZE - 1)
                    else:
                        aval_init_1 = plt.axvline(
                            x=aval_init[0] + tolerance_visibility,
                            color='cyan',
                            linestyle="-.")
                        aval_init_2 = plt.axvline(x=aval_init[1], color='cyan',
                                                  linestyle="-.")

                        aval_end_1 = plt.axvline(x=aval_end[0] + 1,
                                                 color='orange',
                                                 linestyle="-.")
                        aval_end_2 = plt.axvline(
                            x=aval_end[1] + 1 - tolerance_visibility,
                            color='orange', linestyle="-.")

                        plt.legend([aval_init_2, aval_end_2],
                                   ['Start availability', 'End availability'],
                                   loc='lower center', ncol=2,
                                   fontsize=LABEL_FONT_SIZE)
                if COLOR is True:
                    avail_pos, avail_neg = get_availability_duration(flex_pos,
                                                                     flex_neg)

                    rgba_colors_pos, rgba_colors_neg = color_availability(
                        avail_pos, avail_neg, plt, unit=device,
                        model_type='ulbig')

                    plt.bar(t_steps, flex_pos, width, color=rgba_colors_pos)
                    plt.bar(t_steps, flex_neg, width, color=rgba_colors_neg)
                else:
                    pos = plt.bar(t_steps, flex_pos, width, color='0.8')
                    neg = plt.bar(t_steps, flex_neg, width, color='0.4')
                    plt.legend((pos[0], neg[0]), (
                        'Positive flexibility', 'Negative flexibility'),
                               loc='lower center', ncol=2,
                               fontsize=LABEL_FONT_SIZE)
                plt.ylim([y_min, y_max])
                plt.ylabel(y_label, fontsize=LABEL_FONT_SIZE, fontweight=LABEL_STRENGTH)
                plt.xlim([0, t_max])
                plt.xticks(np.linspace(0, x_max, x_ticks_count),
                           fontsize=TICKS_FONT_SIZE)
                plt.yticks(fontsize=TICKS_FONT_SIZE)
                plt.xlabel('Time steps à 15 minutes', fontsize=LABEL_FONT_SIZE, fontweight=LABEL_STRENGTH)
                plt.grid(which='both', color='0.5', linestyle='-',
                         linewidth=0.5)

                if PLOT_TITLE is True:
                    plt.title('Ulbig flexibility ' + plot_title,
                              fontsize=LABEL_FONT_SIZE)
                plt.tight_layout()
                pdf.savefig()

                # if STORE_SINGLE_PLOTS is True:
                #     plt.savefig(
                #         'visualisation/plots/Ulbig_Flex_' + str(
                #             device) + '.png',
                #         dpi=300)

                # 3D flex plots load devices
                threed_flex_pos, threed_flex_neg = threed_plot_ulbig_data(
                    flex_pos, flex_neg, device, t_max)
                threedplot(threed_flex_pos, threed_flex_neg, device, x_max,
                           t_max, x_ticks_count, pdf)

            # -----------Storage devices-----------
            # Plots of storage devices
            elif d_type == 'storage':

                # Assignment of variables
                opt_plan = Ulbig_res['ulbig_devices'][device][
                               'opt_plan'] / normal_fac
                min_plan = -Ulbig_res['ulbig_flex'][device][
                    'u_load_fea_max'] / normal_fac
                max_plan = Ulbig_res['ulbig_flex'][device][
                               'u_gen_fea_max'] / normal_fac
                flex_pos = Ulbig_res['ulbig_flex'][device][
                               'pi_gen_pos'] / normal_fac
                flex_neg = -Ulbig_res['ulbig_flex'][device][
                    'pi_load_pos'] / normal_fac
                opt_plan_pos = np.zeros((t_max))
                opt_plan_neg = np.zeros((t_max))
                for t_step, power in enumerate(opt_plan):
                    if power > 0:
                        opt_plan_pos[t_step] = power
                    if power < 0:
                        opt_plan_neg[t_step] = power
                t_steps = np.arange(len(opt_plan)) + 0.5
                y_label = y_labeling

                # Deviation from opti plan plot storage devices
                y_max = 1.3 * y_scaling
                y_min = -y_max
                plt.figure(figsize=(15, 5))
                if COLOR is True:
                    avail_pos, avail_neg = get_availability_duration(flex_pos,
                                                                     flex_neg)
                    rgba_colors_pos, rgba_colors_neg = color_availability(
                        avail_pos, avail_neg, plt, unit=device,
                        model_type='ulbig')
                    maxi = plt.bar(t_steps, max_plan, width,
                                   color=rgba_colors_pos)
                    plt.bar(t_steps, min_plan, width,
                            color=rgba_colors_neg)
                    plt.bar(t_steps, opt_plan_pos, width,
                            color=rgba_colors_neg)
                    plt.bar(t_steps, opt_plan_neg, width,
                            color=rgba_colors_pos)

                    opti_line = plt.step(np.append(t_steps - 0.5, t_max),
                                         np.append(opt_plan, 0),
                                         linewidth=2, where='post', color='k')
                    plt.legend([opti_line[0]], ['Operation schedule'],
                               loc='lower right',
                               fontsize=LABEL_FONT_SIZE - 1)
                else:
                    maxi = plt.bar(t_steps, max_plan, width, color='0.8')
                    mini = plt.bar(t_steps, min_plan, width, color='0.4')
                    plt.bar(t_steps, opt_plan_pos, width,
                            color='0.4')
                    plt.bar(t_steps, opt_plan_neg, width,
                            color='0.8')
                    opti_line = plt.step(np.append(t_steps - 0.5, t_max),
                                         np.append(opt_plan, 0),
                                         linewidth=2, where='post', color='k')
                    plt.legend((maxi[0], mini[0], opti_line[0]),
                               ('Positive Flexibility',
                                'Negative Flexibility',
                                'Operation schedule'), loc='lower center',
                               ncol=3,
                               fontsize=LABEL_FONT_SIZE)
                plt.ylim([y_min, y_max])
                plt.ylabel(y_label, fontsize=LABEL_FONT_SIZE, fontweight=LABEL_STRENGTH)
                plt.xlim([0, t_max])
                plt.xticks(np.linspace(0, x_max, x_ticks_count),
                           fontsize=TICKS_FONT_SIZE)
                plt.yticks(fontsize=TICKS_FONT_SIZE)
                plt.xlabel('Time steps à 15 minutes', fontsize=LABEL_FONT_SIZE, fontweight=LABEL_STRENGTH)

                if PLOT_TITLE is True:
                    plt.title('Ulbig var ' + plot_title,
                              fontsize=LABEL_FONT_SIZE)

                plt.grid(which='both', color='0.5', linestyle='-',
                         linewidth=0.5)
                plt.tight_layout()
                pdf.savefig()

                # if STORE_SINGLE_PLOTS is True:
                #     plt.savefig(
                #         'visualisation/plots/Ulbig_Dev_' + str(
                #             device) + '.png',
                #         dpi=300)

                # Simple flex plot storage devices
                y_max = max(1.3 * max(flex_pos), -0.3 * (min(flex_neg)))
                y_min = min(1.4 * min(flex_neg), -0.4 * (max(flex_pos)))
                plt.figure(figsize=(15, 5))

                if COLOR is True:
                    avail_pos, avail_neg = get_availability_duration(flex_pos,
                                                                     flex_neg)

                    rgba_colors_pos, rgba_colors_neg = color_availability(
                        avail_pos, avail_neg, plt, unit=device,
                        model_type='ulbig')

                    plt.bar(t_steps, flex_pos, width, color=rgba_colors_pos)
                    plt.bar(t_steps, flex_neg, width, color=rgba_colors_neg)

                else:
                    pos = plt.bar(t_steps, flex_pos, width, color='0.8')
                    neg = plt.bar(t_steps, flex_neg, width, color='0.4')
                    plt.legend((pos[0], neg[0]), (
                        'Positive flexibility', 'Negative flexibility'),
                               loc='lower center', ncol=2,
                               fontsize=LABEL_FONT_SIZE)
                plt.xlim([0, t_max])
                plt.xticks(np.linspace(0, x_max, x_ticks_count),
                           fontsize=TICKS_FONT_SIZE)
                plt.yticks(fontsize=TICKS_FONT_SIZE)
                plt.xlabel('Time steps à 15 minutes', fontsize=LABEL_FONT_SIZE, fontweight=LABEL_STRENGTH)
                plt.ylim([y_min, y_max])
                plt.ylabel(y_label, fontsize=LABEL_FONT_SIZE, fontweight=LABEL_STRENGTH)

                if PLOT_TITLE is True:
                    plt.title('Ulbig flexibility ' + plot_title,
                              fontsize=LABEL_FONT_SIZE)
                plt.grid(which='both', color='0.5', linestyle='-',
                         linewidth=0.5)
                plt.tight_layout()
                pdf.savefig()

                # if STORE_SINGLE_PLOTS is True:
                #     plt.savefig(
                #         'visualisation/plots/Ulbig_Flex_' + str(
                #             device) + '.png',
                #         dpi=300)

                # 3D flex plots storage devices
                threed_flex_pos, threed_flex_neg = threed_plot_ulbig_data(
                    flex_pos, flex_neg, device, t_max)
                threedplot(threed_flex_pos, threed_flex_neg, device, x_max,
                           t_max, x_ticks_count, pdf)

        pdf.close()
        if platform.system() == 'Darwin':  # macOS
            subprocess.call(('open', filepath_fo))
        elif platform.system() == 'Windows':  # Windows
            os.startfile(filepath_fo)
        else:  # linux variants
            os.system('/usr/bin/xdg-open %s 2>/dev/null' % filepath_fo)
    else:
        plot_res = 'Ulbig flexibility visualisation turned off.'

    return plot_res


# 3D plot function
def threedplot(threed_flex_pos, threed_flex_neg, device, x_max, t_max,
               x_ticks_count, pdf):
    if np.mean(threed_flex_neg['dz_neg']) != 0:
        fig, (flexneg3d) = plt.subplots(1, 1, figsize=(15, 15),
                                        subplot_kw=dict(projection='3d'))
        flexneg3d.set_title('OpenTUM 3d negative flexibility ' + device)
        x3_neg = threed_flex_neg['x3_neg']
        y3_neg = threed_flex_neg['y3_neg']
        z3_neg = threed_flex_neg['z3_neg']
        dx_neg = threed_flex_neg['dx_neg']
        dy_neg = threed_flex_neg['dy_neg']
        dz_neg = threed_flex_neg['dz_neg']
        zo_neg = threed_flex_neg['zo_neg']
        bars = np.empty(x3_neg.shape, dtype=object)
        for i, (x3, y3, z3, dx, dy, dz, o) in enumerate(
                ravzip(x3_neg, y3_neg, z3_neg, dx_neg, dy_neg, dz_neg,
                       zo_neg)):
            j, k = divmod(i, t_max)
            if dz != 0:
                bars[j, k] = pl = flexneg3d.bar3d(x3, y3, z3, dx, dy, dz,
                                                  color='0.4')
            else:
                bars[j, k] = pl = flexneg3d.bar3d(x3, y3, z3, dx, dy, dz,
                                                  color='1', alpha=0)
            pl._sort_zpos = o
        flexneg3d.set_xlim(0, t_max)
        flexneg3d.set_xticks(np.linspace(0, x_max, x_ticks_count))
        flexneg3d.set_xlabel('Time steps à 15 minutes', fontsize=14)
        flexneg3d.set_ylabel(
            'Available flexibility duration in time steps à 15 minutes',
            fontsize=14)
        flexneg3d.set_zlabel('Power in kW (EZS)', fontsize=14, labelpad=10)
        flexneg3d.zaxis.set_tick_params(direction='out', pad=8)
        plt.tight_layout()
        pdf.savefig()

        # if STORE_SINGLE_PLOTS is True:
        #     plt.savefig(
        #         'visualisation/plots/Ulbig_3DNeg_' + str(device) + '.png',
        #         dpi=300)
    if np.mean(threed_flex_pos['dz_pos']) != 0:
        fig, (flexpos3d) = plt.subplots(1, 1, figsize=(15, 15),
                                        subplot_kw=dict(projection='3d'))
        flexpos3d.set_title('OpenTUM 3d positive flexibility ' + device)
        x3_pos = threed_flex_pos['x3_pos']
        y3_pos = threed_flex_pos['y3_pos']
        z3_pos = threed_flex_pos['z3_pos']
        dx_pos = threed_flex_pos['dx_pos']
        dy_pos = threed_flex_pos['dy_pos']
        dz_pos = threed_flex_pos['dz_pos']
        zo_pos = threed_flex_pos['zo_pos']
        bars = np.empty(x3_pos.shape, dtype=object)
        for i, (x3, y3, z3, dx, dy, dz, o) in enumerate(
                ravzip(x3_pos, y3_pos, z3_pos, dx_pos, dy_pos, dz_pos,
                       zo_pos)):
            j, k = divmod(i, t_max)
            if dz != 0:
                bars[j, k] = pl = flexpos3d.bar3d(x3, y3, z3, dx, dy, dz,
                                                  color='0.8')
            else:
                bars[j, k] = pl = flexpos3d.bar3d(x3, y3, z3, dx, dy, dz,
                                                  color='1', alpha=0)
            pl._sort_zpos = o
        flexpos3d.set_xlim(0, t_max)
        flexpos3d.set_xticks(np.linspace(0, x_max, x_ticks_count))
        flexpos3d.set_xlabel('Time steps à 15 minutes', fontsize=14)
        flexpos3d.set_ylabel(
            'Available flexibility duration in time steps à 15 minutes',
            fontsize=14)
        flexpos3d.set_zlabel('Power in kW (EZS)', fontsize=14)
        plt.tight_layout()
        pdf.savefig()

        # if STORE_SINGLE_PLOTS is True:
        #     plt.savefig(
        #         'visualisation/plots/Ulbig_3DNeg_' + str(device) + '.png',
        #         dpi=300)


def threed_plot_ulbig_data(flex_pos, flex_neg, device, t_max):
    # Flexible power
    flex_pow_pos = np.array(flex_pos)
    flex_pow_neg = np.array(flex_neg)

    # Depth of 3D graph
    max_l_pos = 10
    max_l_neg = 10

    # Plot arrays positive
    x3_pos = np.zeros((max_l_pos, t_max))
    y3_pos = np.zeros((max_l_pos, t_max))
    z3_pos = np.zeros((max_l_pos, t_max))
    dx_pos = np.zeros((max_l_pos, t_max))
    dy_pos = np.zeros((max_l_pos, t_max))
    dz_pos = np.zeros((max_l_pos, t_max))

    # Plot arrays negative
    x3_neg = np.zeros((max_l_neg, t_max))
    y3_neg = np.zeros((max_l_neg, t_max))
    z3_neg = np.zeros((max_l_neg, t_max))
    dx_neg = np.zeros((max_l_neg, t_max))
    dy_neg = np.zeros((max_l_neg, t_max))
    dz_neg = np.zeros((max_l_neg, t_max))

    # Fill positive plot arrays
    for t_step, power in enumerate(flex_pow_pos):

        if power > 0:
            x3_pos[0][t_step] = t_step
            y3_pos[0][t_step] = 0
            z3_pos[0][t_step] = 0
            dx_pos[0][t_step] = 1
            dy_pos[0][t_step] = 1
            dz_pos[0][t_step] = flex_pow_pos[t_step]
            for flex_step in range(1, max_l_pos):
                x3_pos[flex_step][t_step] = t_step
                y3_pos[flex_step][t_step] = flex_step
                z3_pos[flex_step][t_step] = 0
                dx_pos[flex_step][t_step] = 1
                dy_pos[flex_step][t_step] = 1
                dz_pos[flex_step][t_step] = 0

        if power == 0:
            for flex_step in range(max_l_pos):
                x3_pos[flex_step][t_step] = t_step
                y3_pos[flex_step][t_step] = flex_step
                z3_pos[flex_step][t_step] = 0
                dx_pos[flex_step][t_step] = 1
                dy_pos[flex_step][t_step] = 1
                dz_pos[flex_step][t_step] = 0

    # Fill negative plot arrays
    for t_step, power in enumerate(flex_pow_neg):

        if abs(power) > 0:
            x3_neg[0][t_step] = t_step
            y3_neg[0][t_step] = 0
            z3_neg[0][t_step] = 0
            dx_neg[0][t_step] = 1
            dy_neg[0][t_step] = 1
            dz_neg[0][t_step] = flex_pow_neg[t_step]
            for flex_step in range(1, max_l_neg):
                x3_neg[flex_step][t_step] = t_step
                y3_neg[flex_step][t_step] = flex_step
                z3_neg[flex_step][t_step] = 0
                dx_neg[flex_step][t_step] = 1
                dy_neg[flex_step][t_step] = 1
                dz_neg[flex_step][t_step] = 0

        if power == 0:
            for flex_step in range(max_l_neg):
                x3_neg[flex_step][t_step] = t_step
                y3_neg[flex_step][t_step] = flex_step
                z3_neg[flex_step][t_step] = 0
                dx_neg[flex_step][t_step] = 1
                dy_neg[flex_step][t_step] = 1
                dz_neg[flex_step][t_step] = 0

    # Fill array zo_pos for the right plot order of positive 3D plot
    zo_pos = np.zeros((x3_pos.shape))
    for row, num in enumerate(zo_pos):
        for col in range(len(num)):
            zo_pos[row][col] = -((row + 1) * len(num) - col) + (
                    len(zo_pos) * len(num))

    # Fill array zo_neg for the right plot order of negative 3D plot
    zo_neg = np.zeros((x3_neg.shape))
    for row, num in enumerate(zo_neg):
        for col in range(len(num)):
            zo_neg[row][col] = -((row + 1) * len(num) - col) + (
                    len(zo_neg) * len(num))

    # Fill plot dict
    threed_data_pos = {'x3_pos': x3_pos, 'y3_pos': y3_pos, 'z3_pos': z3_pos,
                       'dx_pos': dx_pos, 'dy_pos': dy_pos, 'dz_pos': dz_pos,
                       'zo_pos': zo_pos}
    threed_data_neg = {'x3_neg': x3_neg, 'y3_neg': y3_neg,
                       'z3_neg': z3_neg, 'dx_neg': dx_neg, 'dy_neg': dy_neg,
                       'dz_neg': dz_neg, 'zo_neg': zo_neg}

    return threed_data_pos, threed_data_neg


def ravzip(*itr):
    # flatten and zip arrays
    return zip(*map(np.ravel, itr))


def get_availability_duration(flex_pos, flex_neg):
    avail_pos = np.array(
        [1 if abs(flex_pos[x]) > 0 else 0 for x in range(len(flex_pos))])
    avail_neg = np.array(
        [1 if abs(flex_neg[x]) > 0 else 0 for x in range(len(flex_neg))])
    return avail_pos, avail_neg
