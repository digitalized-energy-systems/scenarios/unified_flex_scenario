from setuptools import setup

setup(name='visualisation_ufs',
      version='1.0',
      description='Unified flex scenario - Comparing flexibilitiy models',
      url='https://gitlab.com/digitalized-energy-systems/scenarios/unified_flex_scenario',
      author='Jonathan Brandt')
