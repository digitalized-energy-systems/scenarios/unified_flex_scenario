import math

import matplotlib as mpl
import numpy as np
from matplotlib.cm import ScalarMappable
from pyomo.common.dependencies import matplotlib

# create own color map or choose from:
# https://matplotlib.org/stable/tutorials/colors/colormaps.html

# color maps for negative (CMAP_NEG) and positive (CMAP_POS) availability of
# flexibility
CMAP_NEG = matplotlib.colors.LinearSegmentedColormap.from_list("",
                                                               ['#3de2dd',
                                                                '#45d7dd',
                                                                '#4bcddd',
                                                                '#4fc3dd',
                                                                '#52b8dd',
                                                                '#54aedd',
                                                                '#55a4dd',
                                                                '#559add',
                                                                '#558fdc',
                                                                '#5485dc',
                                                                '#527bdc',
                                                                '#4f71db',
                                                                '#4c66db',
                                                                '#485cda',
                                                                '#4351d9',
                                                                '#3d46d9',
                                                                '#353bd8',
                                                                '#2b2ed7',
                                                                '#1d20d7',
                                                                '#000ad6']
                                                               )

CMAP_POS = matplotlib.colors.LinearSegmentedColormap.from_list("", ['#ffe40d',
                                                                    '#ffda19',
                                                                    '#ffcf1f',
                                                                    '#ffc423',
                                                                    '#ffb924',
                                                                    '#fdaf24',
                                                                    '#fba424',
                                                                    '#f89a24',
                                                                    '#f49123',
                                                                    '#f08723',
                                                                    '#ea7e23',
                                                                    '#e57523',
                                                                    '#de6d24',
                                                                    '#d86424',
                                                                    '#d15c25',
                                                                    '#ca5426',
                                                                    '#c34b27',
                                                                    '#bc4328',
                                                                    '#b43b28',
                                                                    '#ad3329',
                                                                    '#a52a2a'])
# Color for operation schedule, if COLOR is set to True
OPTI_PLAN_COLOR = '1'

# Colors for the end and the beginning of the availability duration for
# electric vehicles, if COLOR is set to True
EV_AVAILABILITY_END_COLOR = 'black'
EV_AVAILABILITY_BEGIN_COLOR = 'black'

# If COLOR is set to True, the plots containing the flexibility are colored
# according to the availability of the positive and negative flexibility.
COLOR = True

# If USE_SAME_RANGE is True, for OpenTUM and Ulbig for plotting the
# availability duration, the same scale is used
USE_SAME_RANGE = True
if USE_SAME_RANGE:
    VAL_EV = 25
    VAL_HP = 5

# set sizes of labels and ticks
if COLOR:
    TICKS_FONT_SIZE = 14
    LABEL_FONT_SIZE = 20
else:
    TICKS_FONT_SIZE = 10
    LABEL_FONT_SIZE = 14

# if PLOT_TITLE is True, each single plot contains a title
PLOT_TITLE = False

# if STORE_SINGLE_PLOTS is True, the plots are not only stored as complete
# pdf per approach, but each plot contained in the pdf is stored as separate
# png-file
STORE_SINGLE_PLOTS = True

# choose 'bold' for fat labels and 'normal' for normal ones
LABEL_STRENGTH = 'normal'


def plt_title(device):
    if device == 'hp':
        plot_title = 'Heat Pump'
    elif device == 'ev':
        plot_title = 'Electric Vehicle'
    elif device == 'chp':
        plot_title = 'Combined Heat and Power Unit'
    elif device == 'pv':
        plot_title = 'Photovoltaic System'
    elif device == 'bat':
        plot_title = 'Battery Storage'

    return plot_title


def normalised_plots_OpenTUM(device, OpenTUM_res):
    normalised_plots_OpenTUM = True

    if normalised_plots_OpenTUM:
        y_scaling = 1
        y_labeling = 'Power (ASC)/Nominal Power'
        if device == 'hp':
            # norm_fac = np.array(OpenTUM_res['optplan']['HP_ele_run'])
            norm_fac = np.array(OpenTUM_res['devices'][device]['maxpow'])
        else:
            norm_fac = np.array(OpenTUM_res['devices'][device]['maxpow'])
    else:
        y_scaling = OpenTUM_res['devices'][device]['maxpow']
        y_labeling = 'Power in kW (ASC)'

    plot_title = plt_title(device)

    return norm_fac, y_scaling, y_labeling, plot_title


def normalised_plots_Ulbig(device, OpenTUM_res, Ulbig_res, d_type):
    normalised_plots_Ulbig = True

    if normalised_plots_Ulbig:
        y_scaling = 1
        y_labeling = 'Power (ASC)/Nominal Power'
        if device == 'hp':
            # norm_fac = np.array(OpenTUM_res['optplan']['HP_ele_run'])
            norm_fac = np.array(OpenTUM_res['devices'][device]['maxpow'])
        else:
            norm_fac = np.array(OpenTUM_res['devices'][device]['maxpow'])

    else:
        y_labeling = 'Power in kW (ASC)'
        if d_type == 'generator' or d_type == 'storage':
            y_scaling = Ulbig_res['ulbig_devices'][device]['u_gen,max']
        else:
            y_scaling = Ulbig_res['ulbig_devices'][device]['u_load,max']

    plot_title = plt_title(device)

    return norm_fac, y_scaling, y_labeling, plot_title


def normalised_plots_FlexOffer(device, OpenTUM_res):
    normalised_plots_FlexOffer = True

    if normalised_plots_FlexOffer:
        y_scaling = 1
        y_labeling = 'Power (ASC)/Nominal Power'
        if device == 'hp':
            # norm_fac = np.array(OpenTUM_res['optplan']['HP_ele_run'])
            norm_fac = np.array(OpenTUM_res['devices'][device]['maxpow'])
        else:
            norm_fac = np.array(OpenTUM_res['devices'][device]['maxpow'])
    else:
        y_scaling = OpenTUM_res['devices'][device]['maxpow']
        y_labeling = 'Power in kW (ASC)'

    plot_title = plt_title(device)

    return norm_fac, y_scaling, y_labeling, plot_title


def create_color_list(avail_pos, avail_neg, model_type, unit):
    """
    Create lists of colors according to color maps and given availabilities
    for negative and positive flexibility.
    """
    # Determine maximum values of availability
    max_avail_pos = max(avail_pos)
    max_avail_neg = max(avail_neg)

    if (model_type == 'ulbig' or model_type == 'tum') and USE_SAME_RANGE:
        if unit == 'ev':
            max_avail_pos = VAL_EV
            max_avail_neg = VAL_EV
        elif unit == 'hp':
            max_avail_pos = VAL_HP
            max_avail_neg = VAL_HP

    # Create list of values for the color according to value of availability 
    # for the positive flexibility
    if all(x == avail_pos[0] for x in avail_pos):
        if avail_pos[0] == 0:
            rgba_colors_pos = CMAP_POS(0)
        else:
            rgba_colors_pos = CMAP_POS(1)
    else:
        rgba_colors_pos = [
            CMAP_POS(i / round_up_value(max_avail_pos)) if i != 0 else
            CMAP_POS(0)
            for i in avail_pos]

    # create a color list for the negative flexibility
    if all(x == avail_neg[0] for x in avail_neg):
        if avail_neg[0] == 0:
            rgba_colors_neg = CMAP_NEG(0)
        else:
            rgba_colors_neg = CMAP_NEG(1)
    else:
        rgba_colors_neg = [
            CMAP_NEG(
                i / round_up_value(max_avail_neg)) if i != 0 else CMAP_NEG(0)
            for i in avail_neg]

    return rgba_colors_pos, rgba_colors_neg


def round_up_value(value):
    """
    Method to round up parameter n to a certain value. Normally, it uses
    the next even number.

    """
    # if number is smaller than 5, set it to five to not have too small numbers
    # in the legend of the plot
    if value < 5:
        number = 5
    elif value % 2 == 0:
        number = int(value)
    else:
        number = int(value) + 1

    # some extra constraints to scale plots correctly
    if 20 <= number <= 25:
        number = 25
    if number > 25:
        number = 5

    multiplier = number ** -1
    return int(math.ceil(value * multiplier) / multiplier)


def color_availability(avail_pos, avail_neg, plt, model_type, unit):
    """
    Color existing plot (plt) with flexibilities in colors of the availability
    for the given flexibility. 
    """
    avail_pos = avail_pos.astype(np.int)
    avail_neg = avail_neg.astype(np.int)

    max_val_pos = round_up_value(max(avail_pos))
    max_val_neg = round_up_value(max(avail_neg))

    # If scaling all plots in the same availability range, use
    # the same value when calling Normalize for parameter vmax.
    # if use_same_range, the set values are used
    if (model_type == 'ulbig' or model_type == 'tum') and USE_SAME_RANGE:
        if unit == 'ev':
            max_val_pos = VAL_EV
            max_val_neg = VAL_EV
        elif unit == 'hp':
            max_val_pos = VAL_HP
            max_val_neg = VAL_HP

    # Create color lists
    rgba_colors_pos, rgba_colors_neg = create_color_list(
        avail_pos=avail_pos,
        avail_neg=avail_neg,
        model_type=model_type,
        unit=unit)

    # Both lists only contain one value or one value and 0
    if ((0 in avail_pos and len(set(avail_pos)) == 2) or (
            all(x == 0 for x in avail_pos) and avail_pos[0] != 0)) \
            and ((0 in avail_neg and len(set(avail_neg)) == 2) or (
            all(x == 0 for x in avail_neg) and avail_neg[0] != 0)):
        norm = mpl.colors.Normalize(vmin=0,
                                    vmax=max_val_neg)
        sm = ScalarMappable(cmap=CMAP_NEG, norm=norm)
        sm.set_array(avail_neg)

        cbar = plt.colorbar(sm, location='right', fraction=0.05)
        cbar.ax.tick_params(labelsize=TICKS_FONT_SIZE)
        cbar.set_label('$\it{Availability\ duration}$ in time steps\nfor negative flexibility',
                       fontsize=LABEL_FONT_SIZE-3)

        # scale to own flex
        norm = mpl.colors.Normalize(vmin=0,
                                    vmax=max_val_pos)
        sm = ScalarMappable(cmap=CMAP_POS, norm=norm)
        sm.set_array(avail_neg)

        cbar = plt.colorbar(sm, location='right', fraction=0.05)
        cbar.ax.tick_params(labelsize=TICKS_FONT_SIZE)
        cbar.set_label('$\it{Availability\ duration}$ in time steps\nfor positive flexibility',
                       fontsize=LABEL_FONT_SIZE-3)
        return rgba_colors_pos, rgba_colors_neg

    if not all(x == 0 for x in avail_neg):
        # if there is only one value (or two and one of them is 0)
        # in negative availability, use range for positive availability
        if all(x == avail_neg[0] for x in avail_neg) or 0 in avail_neg and len(
                set(avail_neg)) == 2:
            if not all(x == 0 for x in avail_pos):
                rgba_colors_neg = [
                    CMAP_NEG(i / max_val_pos) if i != 0
                    else CMAP_NEG(
                        0)
                    for i in avail_pos]

                norm = mpl.colors.Normalize(vmin=0,
                                            vmax=max_val_pos)
                sm = ScalarMappable(cmap=CMAP_NEG, norm=norm)
                sm.set_array(avail_pos)

                cbar = plt.colorbar(sm, location='right', fraction=0.05)
                cbar.ax.tick_params(labelsize=TICKS_FONT_SIZE)
                cbar.set_label('$\it{Availability\ duration}$ in time steps\nfor negative flexibility',
                               fontsize=LABEL_FONT_SIZE-3)

                norm = mpl.colors.Normalize(vmin=0,
                                            vmax=max_val_pos)
                sm = ScalarMappable(cmap=CMAP_POS, norm=norm)
                sm.set_array(avail_pos)

                cbar = plt.colorbar(sm, location='right', fraction=0.05)
                cbar.ax.tick_params(labelsize=TICKS_FONT_SIZE)
                cbar.set_label('$\it{Availability\ duration}$ in time steps\nfor positive flexibility',
                               fontsize=LABEL_FONT_SIZE-3)
            else:
                norm = mpl.colors.Normalize(vmin=0,
                                            vmax=max_val_neg)
                sm = ScalarMappable(cmap=CMAP_NEG, norm=norm)
                sm.set_array(avail_neg)

                cbar = plt.colorbar(sm, location='right', fraction=0.05)
                cbar.ax.tick_params(labelsize=TICKS_FONT_SIZE)
                cbar.set_label('$\it{Availability\ duration}$ in time steps\nfor negative flexibility',
                               fontsize=LABEL_FONT_SIZE-3)

            return rgba_colors_pos, rgba_colors_neg

        # plot negative availability
        norm = mpl.colors.Normalize(vmin=0,
                                    vmax=max_val_neg)
        sm = ScalarMappable(cmap=CMAP_NEG, norm=norm)
        sm.set_array(avail_neg)

        cbar = plt.colorbar(sm, location='right', fraction=0.05)
        cbar.ax.tick_params(labelsize=TICKS_FONT_SIZE)
        cbar.set_label('$\it{Availability\ duration}$ in time steps\nfor negative flexibility',
                       fontsize=LABEL_FONT_SIZE-3)

    if not all(x == 0 for x in avail_pos):
        if ((all(x == avail_pos[0] for x in avail_pos)) or (
                0 in avail_pos and len(set(avail_pos)) == 2)) and \
                not all(x == 0 for x in avail_neg):
            # if there is only one value (or two and one of them is 0)
            # in availability of positive flexibility, use range for
            # availability of negative availability
            rgba_colors_pos = [
                CMAP_POS(i / max_val_neg) if i != 0 else CMAP_POS(
                    0)
                for i in avail_neg]

            norm = mpl.colors.Normalize(vmin=0,
                                        vmax=max_val_neg)
            sm = ScalarMappable(cmap=CMAP_POS, norm=norm)
            sm.set_array(avail_neg)

            cbar = plt.colorbar(sm, location='right', fraction=0.05)
            cbar.ax.tick_params(labelsize=TICKS_FONT_SIZE)
            cbar.set_label('$\it{Availability\ duration}$ in time steps\nfor positive flexibility',
                           fontsize=LABEL_FONT_SIZE-3)
        else:
            # plot availability of positive flexibility
            norm = mpl.colors.Normalize(vmin=0,
                                        vmax=max_val_pos)
            sm = ScalarMappable(cmap=CMAP_POS, norm=norm)
            sm.set_array(avail_pos)

            cbar = plt.colorbar(sm, location='right', fraction=0.05)
            cbar.ax.tick_params(labelsize=TICKS_FONT_SIZE)
            cbar.set_label('$\it{Availability\ duration}$ in time steps\nfor positive flexibility',
                           fontsize=LABEL_FONT_SIZE-3)

    return rgba_colors_pos, rgba_colors_neg
